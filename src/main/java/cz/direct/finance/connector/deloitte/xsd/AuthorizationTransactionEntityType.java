//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.11.20 at 09:18:30 AM CET 
//


package cz.direct.finance.connector.deloitte.xsd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for AuthorizationTransactionEntityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AuthorizationTransactionEntityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AuthorizationTransaction_5910" type="{}AuthorizationTransaction_5910Type"/>
 *         &lt;element name="CustomFinancialData" type="{}CustomFinancialDataType" maxOccurs="30" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ProcessorTransactionId" use="required" type="{}Id50Type" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuthorizationTransactionEntityType", propOrder = {
    "authorizationTransaction5910",
    "customFinancialData"
})
public class AuthorizationTransactionEntityType {

    @XmlElement(name = "AuthorizationTransaction_5910", required = true)
    protected AuthorizationTransaction5910Type authorizationTransaction5910;
    @XmlElement(name = "CustomFinancialData")
    protected List<CustomFinancialDataType> customFinancialData;
    @XmlAttribute(name = "ProcessorTransactionId", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String processorTransactionId;

    /**
     * Gets the value of the authorizationTransaction5910 property.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationTransaction5910Type }
     *     
     */
    public AuthorizationTransaction5910Type getAuthorizationTransaction5910() {
        return authorizationTransaction5910;
    }

    /**
     * Sets the value of the authorizationTransaction5910 property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationTransaction5910Type }
     *     
     */
    public void setAuthorizationTransaction5910(AuthorizationTransaction5910Type value) {
        this.authorizationTransaction5910 = value;
    }

    /**
     * Gets the value of the customFinancialData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customFinancialData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomFinancialData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomFinancialDataType }
     * 
     * 
     */
    public List<CustomFinancialDataType> getCustomFinancialData() {
        if (customFinancialData == null) {
            customFinancialData = new ArrayList<CustomFinancialDataType>();
        }
        return this.customFinancialData;
    }

    /**
     * Gets the value of the processorTransactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessorTransactionId() {
        return processorTransactionId;
    }

    /**
     * Sets the value of the processorTransactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessorTransactionId(String value) {
        this.processorTransactionId = value;
    }

}
