//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.11.20 at 09:18:30 AM CET 
//


package cz.direct.finance.connector.deloitte.xsd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GlobalInvoiceEntityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GlobalInvoiceEntityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GlobalInvoice_5100" type="{}GlobalInvoice_5100Type"/>
 *         &lt;element name="TaxAddendum_5300" type="{}TaxAddendum_5300Type" maxOccurs="6" minOccurs="0"/>
 *         &lt;element name="CustomFinancialData" type="{}CustomFinancialDataType" maxOccurs="30" minOccurs="0"/>
 *         &lt;element name="LineItemDetailEntity" type="{}LineItemDetailEntityType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GlobalInvoiceEntityType", propOrder = {
    "globalInvoice5100",
    "taxAddendum5300",
    "customFinancialData",
    "lineItemDetailEntity"
})
public class GlobalInvoiceEntityType {

    @XmlElement(name = "GlobalInvoice_5100", required = true)
    protected GlobalInvoice5100Type globalInvoice5100;
    @XmlElement(name = "TaxAddendum_5300")
    protected List<TaxAddendum5300Type> taxAddendum5300;
    @XmlElement(name = "CustomFinancialData")
    protected List<CustomFinancialDataType> customFinancialData;
    @XmlElement(name = "LineItemDetailEntity")
    protected List<LineItemDetailEntityType> lineItemDetailEntity;

    /**
     * Gets the value of the globalInvoice5100 property.
     * 
     * @return
     *     possible object is
     *     {@link GlobalInvoice5100Type }
     *     
     */
    public GlobalInvoice5100Type getGlobalInvoice5100() {
        return globalInvoice5100;
    }

    /**
     * Sets the value of the globalInvoice5100 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GlobalInvoice5100Type }
     *     
     */
    public void setGlobalInvoice5100(GlobalInvoice5100Type value) {
        this.globalInvoice5100 = value;
    }

    /**
     * Gets the value of the taxAddendum5300 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxAddendum5300 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaxAddendum5300().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TaxAddendum5300Type }
     * 
     * 
     */
    public List<TaxAddendum5300Type> getTaxAddendum5300() {
        if (taxAddendum5300 == null) {
            taxAddendum5300 = new ArrayList<TaxAddendum5300Type>();
        }
        return this.taxAddendum5300;
    }

    /**
     * Gets the value of the customFinancialData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customFinancialData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomFinancialData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomFinancialDataType }
     * 
     * 
     */
    public List<CustomFinancialDataType> getCustomFinancialData() {
        if (customFinancialData == null) {
            customFinancialData = new ArrayList<CustomFinancialDataType>();
        }
        return this.customFinancialData;
    }

    /**
     * Gets the value of the lineItemDetailEntity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItemDetailEntity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItemDetailEntity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LineItemDetailEntityType }
     * 
     * 
     */
    public List<LineItemDetailEntityType> getLineItemDetailEntity() {
        if (lineItemDetailEntity == null) {
            lineItemDetailEntity = new ArrayList<LineItemDetailEntityType>();
        }
        return this.lineItemDetailEntity;
    }

}
