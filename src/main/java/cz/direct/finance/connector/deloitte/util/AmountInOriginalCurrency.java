package cz.direct.finance.connector.deloitte.util;

import java.util.Map;
import java.util.TreeMap;

class AmountInOriginalCurrency {

    private static Map<String, String[]> map = new TreeMap<>();

    static {
        map.put("AED", new String[]{"784", "2"});
        map.put("AFN", new String[]{"971", "2"});
        map.put("ALL", new String[]{"008", "2"});
        map.put("AMD", new String[]{"051", "2"});
        map.put("ANG", new String[]{"532", "2"});
        map.put("AOA", new String[]{"973", "2"});
        map.put("ARS", new String[]{"032", "2"});
        map.put("AUD", new String[]{"036", "2"});
        map.put("AWG", new String[]{"533", "2"});
        map.put("AZN", new String[]{"944", "2"});
        map.put("BAM", new String[]{"977", "2"});
        map.put("BBD", new String[]{"052", "2"});
        map.put("BDT", new String[]{"050", "2"});
        map.put("BGN", new String[]{"975", "2"});
        map.put("BHD", new String[]{"048", "3"});
        map.put("BIF", new String[]{"108", "0"});
        map.put("BMD", new String[]{"060", "2"});
        map.put("BND", new String[]{"096", "2"});
        map.put("BOB", new String[]{"068", "2"});
        map.put("BOV", new String[]{"984", "2"});
        map.put("BRL", new String[]{"986", "2"});
        map.put("BSD", new String[]{"044", "2"});
        map.put("BTN", new String[]{"064", "2"});
        map.put("BWP", new String[]{"072", "2"});
        map.put("BYN", new String[]{"933", "2"});
        map.put("BZD", new String[]{"084", "2"});
        map.put("CAD", new String[]{"124", "2"});
        map.put("CDF", new String[]{"976", "2"});
        map.put("CHE", new String[]{"947", "2"});
        map.put("CHF", new String[]{"756", "2"});
        map.put("CHW", new String[]{"948", "2"});
        map.put("CLF", new String[]{"990", "4"});
        map.put("CLP", new String[]{"152", "0"});
        map.put("CNY", new String[]{"156", "2"});
        map.put("COP", new String[]{"170", "2"});
        map.put("COU", new String[]{"970", "2"});
        map.put("CRC", new String[]{"188", "2"});
        map.put("CUC", new String[]{"931", "2"});
        map.put("CUP", new String[]{"192", "2"});
        map.put("CVE", new String[]{"132", "0"});
        map.put("CZK", new String[]{"203", "2"});
        map.put("DJF", new String[]{"262", "0"});
        map.put("DKK", new String[]{"208", "2"});
        map.put("DOP", new String[]{"214", "2"});
        map.put("DZD", new String[]{"012", "2"});
        map.put("EGP", new String[]{"818", "2"});
        map.put("ERN", new String[]{"232", "2"});
        map.put("ETB", new String[]{"230", "2"});
        map.put("EUR", new String[]{"978", "2"});
        map.put("FJD", new String[]{"242", "2"});
        map.put("FKP", new String[]{"238", "2"});
        map.put("GBP", new String[]{"826", "2"});
        map.put("GEL", new String[]{"981", "2"});
        map.put("GHS", new String[]{"936", "2"});
        map.put("GIP", new String[]{"292", "2"});
        map.put("GMD", new String[]{"270", "2"});
        map.put("GNF", new String[]{"324", "0"});
        map.put("GTQ", new String[]{"320", "2"});
        map.put("GYD", new String[]{"328", "2"});
        map.put("HKD", new String[]{"344", "2"});
        map.put("HNL", new String[]{"340", "2"});
        map.put("HRK", new String[]{"191", "2"});
        map.put("HTG", new String[]{"332", "2"});
        map.put("HUF", new String[]{"348", "2"});
        map.put("IDR", new String[]{"360", "2"});
        map.put("ILS", new String[]{"376", "2"});
        map.put("INR", new String[]{"356", "2"});
        map.put("IQD", new String[]{"368", "3"});
        map.put("IRR", new String[]{"364", "2"});
        map.put("ISK", new String[]{"352", "0"});
        map.put("JMD", new String[]{"388", "2"});
        map.put("JOD", new String[]{"400", "3"});
        map.put("JPY", new String[]{"392", "0"});
        map.put("KES", new String[]{"404", "2"});
        map.put("KGS", new String[]{"417", "2"});
        map.put("KHR", new String[]{"116", "2"});
        map.put("KMF", new String[]{"174", "0"});
        map.put("KPW", new String[]{"408", "2"});
        map.put("KRW", new String[]{"410", "0"});
        map.put("KWD", new String[]{"414", "3"});
        map.put("KYD", new String[]{"136", "2"});
        map.put("KZT", new String[]{"398", "2"});
        map.put("LAK", new String[]{"418", "2"});
        map.put("LBP", new String[]{"422", "2"});
        map.put("LKR", new String[]{"144", "2"});
        map.put("LRD", new String[]{"430", "2"});
        map.put("LSL", new String[]{"426", "2"});
        map.put("LTL", new String[]{"440", "2"});
        map.put("LYD", new String[]{"434", "3"});
        map.put("MAD", new String[]{"504", "2"});
        map.put("MDL", new String[]{"498", "2"});
        map.put("MGA", new String[]{"969", "1"});
        map.put("MKD", new String[]{"807", "2"});
        map.put("MMK", new String[]{"104", "2"});
        map.put("MNT", new String[]{"496", "2"});
        map.put("MOP", new String[]{"446", "2"});
        map.put("MRU", new String[]{"929", "1"});
        map.put("MUR", new String[]{"480", "2"});
        map.put("MVR", new String[]{"462", "2"});
        map.put("MWK", new String[]{"454", "2"});
        map.put("MXN", new String[]{"484", "2"});
        map.put("MXV", new String[]{"979", "2"});
        map.put("MYR", new String[]{"458", "2"});
        map.put("MZN", new String[]{"943", "2"});
        map.put("NAD", new String[]{"516", "2"});
        map.put("NGN", new String[]{"566", "2"});
        map.put("NIO", new String[]{"558", "2"});
        map.put("NOK", new String[]{"578", "2"});
        map.put("NPR", new String[]{"524", "2"});
        map.put("NZD", new String[]{"554", "2"});
        map.put("OMR", new String[]{"512", "3"});
        map.put("PAB", new String[]{"590", "2"});
        map.put("PEN", new String[]{"604", "2"});
        map.put("PGK", new String[]{"598", "2"});
        map.put("PHP", new String[]{"608", "2"});
        map.put("PKR", new String[]{"586", "2"});
        map.put("PLN", new String[]{"985", "2"});
        map.put("PYG", new String[]{"600", "0"});
        map.put("QAR", new String[]{"634", "2"});
        map.put("RON", new String[]{"946", "2"});
        map.put("RSD", new String[]{"941", "2"});
        map.put("RUB", new String[]{"643", "2"});
        map.put("RWF", new String[]{"646", "0"});
        map.put("SAR", new String[]{"682", "2"});
        map.put("SBD", new String[]{"090", "2"});
        map.put("SCR", new String[]{"690", "2"});
        map.put("SDG", new String[]{"938", "2"});
        map.put("SEK", new String[]{"752", "2"});
        map.put("SGD", new String[]{"702", "2"});
        map.put("SHP", new String[]{"654", "2"});
        map.put("SLL", new String[]{"694", "2"});
        map.put("SOS", new String[]{"706", "2"});
        map.put("SRD", new String[]{"968", "2"});
        map.put("SSP", new String[]{"728", "2"});
        map.put("STN", new String[]{"930", "2"});
        map.put("SVC", new String[]{"222", "2"});
        map.put("SYP", new String[]{"760", "2"});
        map.put("SZL", new String[]{"748", "2"});
        map.put("THB", new String[]{"764", "2"});
        map.put("TJS", new String[]{"972", "2"});
        map.put("TMT", new String[]{"934", "2"});
        map.put("TND", new String[]{"788", "3"});
        map.put("TOP", new String[]{"776", "2"});
        map.put("TRY", new String[]{"949", "2"});
        map.put("TTD", new String[]{"780", "2"});
        map.put("TWD", new String[]{"901", "2"});
        map.put("TZS", new String[]{"834", "2"});
        map.put("UAH", new String[]{"980", "2"});
        map.put("UGX", new String[]{"800", "0"});
        map.put("USD", new String[]{"840", "2"});
        map.put("USN", new String[]{"997", "2"});
        map.put("UYI", new String[]{"940", "0"});
        map.put("UYU", new String[]{"858", "2"});
        map.put("UYW", new String[]{"927", "4"});
        map.put("UZS", new String[]{"860", "2"});
        map.put("VES", new String[]{"928", "2"});
        map.put("VND", new String[]{"704", "0"});
        map.put("VUV", new String[]{"548", "0"});
        map.put("WST", new String[]{"882", "2"});
        map.put("XAF", new String[]{"950", "0"});
        map.put("XAG", new String[]{"961", "0"});
        map.put("XAU", new String[]{"959", "0"});
        map.put("XBA", new String[]{"955", "0"});
        map.put("XBB", new String[]{"956", "0"});
        map.put("XBC", new String[]{"957", "0"});
        map.put("XBD", new String[]{"958", "0"});
        map.put("XCD", new String[]{"951", "2"});
        map.put("XDR", new String[]{"960", "0"});
        map.put("XOF", new String[]{"952", "0"});
        map.put("XPD", new String[]{"964", "0"});
        map.put("XPF", new String[]{"953", "0"});
        map.put("XPT", new String[]{"962", "0"});
        map.put("XSU", new String[]{"994", "0"});
        map.put("XTS", new String[]{"963", "0"});
        map.put("XUA", new String[]{"965", "0"});
        map.put("XXX", new String[]{"999", "0"});
        map.put("YER", new String[]{"886", "2"});
        map.put("ZAR", new String[]{"710", "2"});
        map.put("ZMW", new String[]{"967", "2"});
    }

    static String getIsoCurrencyCode(String code) {
        if(code == null) return "xxx";

        if (map.containsKey(code.toUpperCase()))
            return map.get(code.toUpperCase())[0];
         else
            throw new RuntimeException("Currency code not found for value: " + code);

    }

    static Short getIsoCurrencyExponent(String code) {
        if (code == null) return 0;
        if (map.containsKey(code.toUpperCase())) {
            return new Short(map.get(code.toUpperCase())[1]);
        } else {
            throw new RuntimeException("Currency exponent not found for value: " + code);
        }
    }

    /**
     * Value is adjusted by Currency exponent. E.g, for czech crowns the currency
     * exponent is 2 so if the value originalAmount is 200 I will return 2000
     * (original amount * 10 ^ currencyExponent ... 20 * 10 ^ 2)
     * @param originalCurrency
     * @param originalAmount
     * @return
     */
    static long adjustValue(String originalCurrency, Double originalAmount) {
        short exponent = getIsoCurrencyExponent(originalCurrency);
        double multipleBy = Math.pow(10, (double) exponent);
        Double result = originalAmount * multipleBy;
        return result.longValue();
    }
}




