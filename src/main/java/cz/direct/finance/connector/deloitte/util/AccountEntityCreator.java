package cz.direct.finance.connector.deloitte.util;

import cz.direct.finance.connector.deloitte.model.CardResponse;
import cz.direct.finance.connector.deloitte.xsd.AcceptanceBrandType;
import cz.direct.finance.connector.deloitte.xsd.AccountEntityType;
import cz.direct.finance.connector.deloitte.xsd.AccountInformation4300Type;
import cz.direct.finance.connector.deloitte.model.Transaction;
import cz.direct.finance.connector.deloitte.model.User;
import cz.direct.finance.connector.deloitte.xsd.BillingType;
import cz.direct.finance.connector.deloitte.xsd.HierarchyRecordHeaderType;
import cz.direct.finance.connector.deloitte.xsd.HierarchyStatusCodeType;


class AccountEntityCreator {

    static AccountEntityType create(Transaction transaction, User user, CardResponse card) {

        AccountEntityType accountEntityType = new AccountEntityType();

        AccountInformation4300Type accountInformation4300Type = new AccountInformation4300Type();

        HierarchyRecordHeaderType hierarchyRecordHeaderType = new HierarchyRecordHeaderType();
        hierarchyRecordHeaderType.setSequenceNum(1);
        hierarchyRecordHeaderType.setStatusCode(card.getCardState().equals("active") ? HierarchyStatusCodeType.A : HierarchyStatusCodeType.I);
        accountInformation4300Type.setHierarchyRecordHeader(hierarchyRecordHeaderType);

        accountInformation4300Type.setReportsTo("");
        accountInformation4300Type.setPostedCurrencyCode("203");
        accountInformation4300Type.setBillingType(BillingType.O);
        accountInformation4300Type.setAcceptanceBrandIdCode(AcceptanceBrandType.MCC);
        accountInformation4300Type.setCorporateProduct("MCO");
        accountInformation4300Type.setAccountTypeCode("C");
        accountInformation4300Type.setExpirationDate(DateParser.toXMLGregCal(card.getExpiration()));
        accountInformation4300Type.setNameLine1(user.getFirstName());
        accountInformation4300Type.setNameLine2(user.getLastName());
        accountInformation4300Type.setNameLocaleCode("ENU");

        accountEntityType.setAccountInformation4300(accountInformation4300Type);

        accountEntityType.setAccountNumber(transaction.getCardMaskedNumber());

        accountEntityType.getFinancialTransactionEntity().add(FinancialTransactionEntityCreator.create(transaction));      // financeTransaction

        return accountEntityType;
    }
}
