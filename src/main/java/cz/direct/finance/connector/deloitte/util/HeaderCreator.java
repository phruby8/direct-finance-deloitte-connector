package cz.direct.finance.connector.deloitte.util;

import cz.direct.finance.connector.deloitte.xsd.FinancialRecordHeaderType;
import cz.direct.finance.connector.deloitte.xsd.RunModeType;
import cz.direct.finance.connector.deloitte.xsd.TransRecordHeaderType;
import cz.direct.finance.connector.deloitte.xsd.TransactionStatusCodeType;
import cz.direct.finance.connector.deloitte.xsd.TransmissionHeader1000Type;

public class HeaderCreator {

    public static TransmissionHeader1000Type createTransmissionHeader(){

        TransmissionHeader1000Type transmissionHeader1000Type = new TransmissionHeader1000Type();
        transmissionHeader1000Type.setFileReferenceNum("9999");
        transmissionHeader1000Type.setCDFVersionNum("3.00");
        transmissionHeader1000Type.setRunModeIndicator(RunModeType.P);
        transmissionHeader1000Type.setProcessorNum("18171");
        transmissionHeader1000Type.setSchemaVersionNum("17.01.00.00");

        TransRecordHeaderType transRecordHeaderType = new TransRecordHeaderType();
        transRecordHeaderType.setSequenceNum(1);
        transmissionHeader1000Type.setTransRecordHeader(transRecordHeaderType);

        return transmissionHeader1000Type;
    }

    static FinancialRecordHeaderType createFinRecHeader(){
        FinancialRecordHeaderType financialRecordHeaderType = new FinancialRecordHeaderType();
        financialRecordHeaderType.setSequenceNum(1);
        financialRecordHeaderType.setMaintenanceCode(TransactionStatusCodeType.A);
        return financialRecordHeaderType;
    }
}
