package cz.direct.finance.connector.deloitte.model.rest;

public class GetTransactionsRequest {

    private String cardId;
    private String from;
    private String to;
    private Integer limit;
    private String offsetToken;

    public GetTransactionsRequest(String cardId, String from, String to, Integer limit, String nextOffsetToken) {
        this.cardId = cardId;
        this.from = from;
        this.to = to;
        this.limit = limit;
        this.offsetToken = nextOffsetToken;
    }

    public String getCardId() {
        return cardId;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public Integer getLimit() {
        return limit;
    }

    public String getOffsetToken() {
        return offsetToken;
    }

    @Override
    public String toString() {
        return "GetTransactionsRequest{" +
                "cardId='" + cardId + '\'' +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", limit=" + limit +
                ", offsetToken='" + offsetToken + '\'' +
                '}';
    }
}
