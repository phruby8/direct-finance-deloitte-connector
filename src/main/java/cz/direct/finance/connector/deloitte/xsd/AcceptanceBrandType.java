//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.11.20 at 09:18:30 AM CET 
//


package cz.direct.finance.connector.deloitte.xsd;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AcceptanceBrandType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AcceptanceBrandType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="MCC"/>
 *     &lt;enumeration value="VIS"/>
 *     &lt;enumeration value="PVL"/>
 *     &lt;enumeration value="AMX"/>
 *     &lt;enumeration value="DCI"/>
 *     &lt;enumeration value="API"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AcceptanceBrandType")
@XmlEnum
public enum AcceptanceBrandType {

    MCC,
    VIS,
    PVL,
    AMX,
    DCI,
    API;

    public String value() {
        return name();
    }

    public static AcceptanceBrandType fromValue(String v) {
        return valueOf(v);
    }

}
