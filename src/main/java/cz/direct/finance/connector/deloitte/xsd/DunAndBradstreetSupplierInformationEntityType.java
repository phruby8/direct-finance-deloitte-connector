//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.11.20 at 09:18:30 AM CET 
//


package cz.direct.finance.connector.deloitte.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DunAndBradstreetSupplierInformationEntityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DunAndBradstreetSupplierInformationEntityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SupplierGeneralInformation_6001" type="{}SupplierGeneralInformation_6001Type"/>
 *         &lt;element name="SupplierAddressInformation_6002" type="{}SupplierAddressInformation_6002Type"/>
 *         &lt;element name="SupplierDiversityInformation_6003" type="{}SupplierDiversityInformation_6003Type"/>
 *         &lt;element name="MasterCardSupplierInformation_6004" type="{}MasterCardSupplierInformation_6004Type"/>
 *       &lt;/sequence>
 *       &lt;attribute name="MerchantLocationId" use="required" type="{}Numeric10Type" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DunAndBradstreetSupplierInformationEntityType", propOrder = {
    "supplierGeneralInformation6001",
    "supplierAddressInformation6002",
    "supplierDiversityInformation6003",
    "masterCardSupplierInformation6004"
})
public class DunAndBradstreetSupplierInformationEntityType {

    @XmlElement(name = "SupplierGeneralInformation_6001", required = true)
    protected SupplierGeneralInformation6001Type supplierGeneralInformation6001;
    @XmlElement(name = "SupplierAddressInformation_6002", required = true)
    protected SupplierAddressInformation6002Type supplierAddressInformation6002;
    @XmlElement(name = "SupplierDiversityInformation_6003", required = true)
    protected SupplierDiversityInformation6003Type supplierDiversityInformation6003;
    @XmlElement(name = "MasterCardSupplierInformation_6004", required = true)
    protected MasterCardSupplierInformation6004Type masterCardSupplierInformation6004;
    @XmlAttribute(name = "MerchantLocationId", required = true)
    protected long merchantLocationId;

    /**
     * Gets the value of the supplierGeneralInformation6001 property.
     * 
     * @return
     *     possible object is
     *     {@link SupplierGeneralInformation6001Type }
     *     
     */
    public SupplierGeneralInformation6001Type getSupplierGeneralInformation6001() {
        return supplierGeneralInformation6001;
    }

    /**
     * Sets the value of the supplierGeneralInformation6001 property.
     * 
     * @param value
     *     allowed object is
     *     {@link SupplierGeneralInformation6001Type }
     *     
     */
    public void setSupplierGeneralInformation6001(SupplierGeneralInformation6001Type value) {
        this.supplierGeneralInformation6001 = value;
    }

    /**
     * Gets the value of the supplierAddressInformation6002 property.
     * 
     * @return
     *     possible object is
     *     {@link SupplierAddressInformation6002Type }
     *     
     */
    public SupplierAddressInformation6002Type getSupplierAddressInformation6002() {
        return supplierAddressInformation6002;
    }

    /**
     * Sets the value of the supplierAddressInformation6002 property.
     * 
     * @param value
     *     allowed object is
     *     {@link SupplierAddressInformation6002Type }
     *     
     */
    public void setSupplierAddressInformation6002(SupplierAddressInformation6002Type value) {
        this.supplierAddressInformation6002 = value;
    }

    /**
     * Gets the value of the supplierDiversityInformation6003 property.
     * 
     * @return
     *     possible object is
     *     {@link SupplierDiversityInformation6003Type }
     *     
     */
    public SupplierDiversityInformation6003Type getSupplierDiversityInformation6003() {
        return supplierDiversityInformation6003;
    }

    /**
     * Sets the value of the supplierDiversityInformation6003 property.
     * 
     * @param value
     *     allowed object is
     *     {@link SupplierDiversityInformation6003Type }
     *     
     */
    public void setSupplierDiversityInformation6003(SupplierDiversityInformation6003Type value) {
        this.supplierDiversityInformation6003 = value;
    }

    /**
     * Gets the value of the masterCardSupplierInformation6004 property.
     * 
     * @return
     *     possible object is
     *     {@link MasterCardSupplierInformation6004Type }
     *     
     */
    public MasterCardSupplierInformation6004Type getMasterCardSupplierInformation6004() {
        return masterCardSupplierInformation6004;
    }

    /**
     * Sets the value of the masterCardSupplierInformation6004 property.
     * 
     * @param value
     *     allowed object is
     *     {@link MasterCardSupplierInformation6004Type }
     *     
     */
    public void setMasterCardSupplierInformation6004(MasterCardSupplierInformation6004Type value) {
        this.masterCardSupplierInformation6004 = value;
    }

    /**
     * Gets the value of the merchantLocationId property.
     * 
     */
    public long getMerchantLocationId() {
        return merchantLocationId;
    }

    /**
     * Sets the value of the merchantLocationId property.
     * 
     */
    public void setMerchantLocationId(long value) {
        this.merchantLocationId = value;
    }

}
