package cz.direct.finance.connector.deloitte.service;

import cz.direct.finance.connector.deloitte.config.ConfigurationProperties;
import cz.direct.finance.connector.deloitte.model.rest.GetCardsRequest;
import cz.direct.finance.connector.deloitte.model.rest.GetCardsResponse;
import cz.direct.finance.connector.deloitte.model.rest.GetTransactionResponse;
import cz.direct.finance.connector.deloitte.model.rest.GetTransactionsRequest;
import cz.direct.finance.connector.deloitte.model.rest.GetUserRequest;
import cz.direct.finance.connector.deloitte.model.rest.GetUserResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.ZonedDateTime;

@Service
public class RestApiService {

    private static final Logger log = LoggerFactory.getLogger(RestApiService.class);

    @Autowired
    private ConfigurationProperties configurationProperties;

    @Value("${log.response}")
    private String logResponse;

    ResponseEntity<GetTransactionResponse> getTranstactions(String offset, ZonedDateTime from, ZonedDateTime to) {
        HttpHeaders httpheaders = new HttpHeaders();
        httpheaders.set("x-api-key", configurationProperties.getApiKey());

        GetTransactionsRequest request = new GetTransactionsRequest(null, from.toString(), to.toString(), 1000, offset); //
        HttpEntity<GetTransactionsRequest> requestEntity = new HttpEntity<>(request, httpheaders);
        log.info("Calling " + configurationProperties.getApiUrl() + "/v1/transaction/get-card-transactions with: " + request);
        ResponseEntity<GetTransactionResponse> response = new RestTemplate(
                getRqstWithTimeout()).exchange(configurationProperties.getApiUrl() + "/v1/transaction/get-card-transactions",
                HttpMethod.POST,
                requestEntity,
                GetTransactionResponse.class
        );
        if ("true".equals(logResponse)) {
            log.info("Obtained: " + response);
        }
        return response;
    }

    ResponseEntity<GetUserResponse> getUser(String userId) {
        HttpHeaders httpheaders = new HttpHeaders();
        httpheaders.set("x-api-key", configurationProperties.getApiKey());

        GetUserRequest request = new GetUserRequest(userId);
        HttpEntity<GetUserRequest> requestEntity = new HttpEntity<>(request, httpheaders);
        log.info("Calling " + configurationProperties.getApiUrl() + "/v1/user/get-user with :" + request);
        ResponseEntity<GetUserResponse> response =  new RestTemplate(
                getRqstWithTimeout()).exchange(
                        configurationProperties.getApiUrl() + "/v1/user/get-user",
                HttpMethod.POST,
                requestEntity,
                GetUserResponse.class
        );
        if ("true".equals(logResponse)) {
            log.info("Obtained: " + response);
        }
        return response;

    }

    ResponseEntity<GetCardsResponse> getCards() {
        HttpHeaders httpheaders = new HttpHeaders();
        httpheaders.set("x-api-key", configurationProperties.getApiKey());
        httpheaders.set("Content-Type", "application/json");

        GetCardsRequest request = new GetCardsRequest("test");

        HttpEntity requestEntity = new HttpEntity<>(request, httpheaders);
        log.info("Calling " + configurationProperties.getApiUrl() + "/v1/card/get-cards");
        ResponseEntity<GetCardsResponse> response = new RestTemplate(getRqstWithTimeout()).exchange(configurationProperties.getApiUrl() + "/v1/card/get-cards", HttpMethod.POST, requestEntity, GetCardsResponse.class);
        if ("true".equals(logResponse)) {
            log.info("Obtained: " + response);
        }
        return response;
    }

    private static SimpleClientHttpRequestFactory getRqstWithTimeout() {
        SimpleClientHttpRequestFactory clientHttpRequestFactory = new SimpleClientHttpRequestFactory();
        //Connect timeout
        clientHttpRequestFactory.setConnectTimeout(10_000);

        //Read timeout
        clientHttpRequestFactory.setReadTimeout(10_000);
        return clientHttpRequestFactory;
    }
}
