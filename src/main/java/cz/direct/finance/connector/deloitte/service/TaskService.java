package cz.direct.finance.connector.deloitte.service;

import cz.direct.finance.connector.deloitte.model.DataToParse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import java.io.File;

@Service
public class TaskService {

    @Autowired
    private DataService dataService;

    @Autowired
    private FileService fileService;

    private static final Logger log = LoggerFactory.getLogger(TaskService.class);

    public void process() {

        // create name based on time, example: output_2019-02-14.xml
        String fileName = fileService.getFileName();

        // process just in case there is no file already
        if (!new File(fileName).exists()) {

            // get Data
            DataToParse dataToParse = dataService.prepareDataFromApi();

            // create xml file
            try {
                fileService.createXmlFile(dataToParse, fileName);
            } catch (JAXBException e) {
                log.error("JAXBException: ", e);
                throw new RuntimeException(e);
            }
        } else {
            log.info("File " + fileName + " already exists.");
        }
    }
}