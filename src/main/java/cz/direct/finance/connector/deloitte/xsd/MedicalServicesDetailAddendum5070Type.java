//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.11.20 at 09:18:30 AM CET 
//


package cz.direct.finance.connector.deloitte.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for MedicalServicesDetailAddendum_5070Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MedicalServicesDetailAddendum_5070Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="FinancialRecordHeader" type="{}FinancialRecordHeaderType"/>
 *         &lt;element name="OrderDate" type="{}MasterCardDateType" minOccurs="0"/>
 *         &lt;element name="ShipDate" type="{}MasterCardDateType" minOccurs="0"/>
 *         &lt;element name="ShipToHealthIndustryNum" type="{}Char25Type" minOccurs="0"/>
 *         &lt;element name="ContractNum" type="{}Char20Type" minOccurs="0"/>
 *         &lt;element name="PriceAdjustmentCode" type="{}PriceAdjustmentType" minOccurs="0"/>
 *         &lt;element name="POLineNum" type="{}Numeric4Type" minOccurs="0"/>
 *         &lt;element name="SpecialCondition" type="{}Char50Type" minOccurs="0"/>
 *         &lt;element name="GeneralText" type="{}Char80Type" minOccurs="0"/>
 *         &lt;element name="ProductNum" type="{}Id19Type"/>
 *         &lt;element name="HealthIdNum" type="{}Char9Type"/>
 *         &lt;element name="GLAccountingCode" type="{}Numeric17Type"/>
 *         &lt;element name="ProductNumQualifier" type="{}ProductNumberQualifierType"/>
 *         &lt;element name="DateOfPurchase" type="{}MasterCardDateType" minOccurs="0"/>
 *         &lt;element name="CustomerCode" type="{}Char49Type" minOccurs="0"/>
 *         &lt;element name="ProductCode" type="{}Char15Type"/>
 *         &lt;element name="ProductCodeQualifier" type="{}ProductCodeQualifierType" minOccurs="0"/>
 *         &lt;element name="ItemDescription" type="{}Char80Type"/>
 *         &lt;element name="ItemQuantity" type="{}NumExp16Type"/>
 *         &lt;element name="ItemUnitOfMeasurementCode" type="{}ItemUnitOfMeasurementType"/>
 *         &lt;element name="UnitPrice" type="{}CurrencyAmountType"/>
 *         &lt;element name="ExtendedItemAmount" type="{}CurrencyAmountType"/>
 *         &lt;element name="DiscountIndicator" type="{}TrueFalseUnknownType" minOccurs="0"/>
 *         &lt;element name="DiscountAmount" type="{}CurrencyAmountType" minOccurs="0"/>
 *         &lt;element name="ItemDiscountRate" type="{}NumExp16Type" minOccurs="0"/>
 *         &lt;element name="CommodityCode" type="{}Char15Type" minOccurs="0"/>
 *         &lt;element name="TotalTaxAmount" type="{}CurrencyAmountType" minOccurs="0"/>
 *         &lt;element name="TypeOfSupply" type="{}TypeOfSupplyType" minOccurs="0"/>
 *         &lt;element name="TaxExemptIndicator" type="{}SimpleCodeType" minOccurs="0"/>
 *         &lt;element name="UniqueVATInvoiceReferenceNum" type="{}Char17Type"/>
 *         &lt;element name="LineItemTotalAmount" type="{}CurrencyAmountType"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MedicalServicesDetailAddendum_5070Type", propOrder = {

})
public class MedicalServicesDetailAddendum5070Type {

    @XmlElement(name = "FinancialRecordHeader", required = true)
    protected FinancialRecordHeaderType financialRecordHeader;
    @XmlElement(name = "OrderDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar orderDate;
    @XmlElement(name = "ShipDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar shipDate;
    @XmlElement(name = "ShipToHealthIndustryNum")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String shipToHealthIndustryNum;
    @XmlElement(name = "ContractNum")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String contractNum;
    @XmlElement(name = "PriceAdjustmentCode")
    @XmlSchemaType(name = "token")
    protected PriceAdjustmentType priceAdjustmentCode;
    @XmlElement(name = "POLineNum")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer poLineNum;
    @XmlElement(name = "SpecialCondition")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String specialCondition;
    @XmlElement(name = "GeneralText")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String generalText;
    @XmlElement(name = "ProductNum", required = true, defaultValue = "--")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String productNum;
    @XmlElement(name = "HealthIdNum", required = true, defaultValue = "--")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String healthIdNum;
    @XmlElement(name = "GLAccountingCode")
    @XmlSchemaType(name = "unsignedLong")
    protected long glAccountingCode;
    @XmlElement(name = "ProductNumQualifier", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String productNumQualifier;
    @XmlElement(name = "DateOfPurchase")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateOfPurchase;
    @XmlElement(name = "CustomerCode")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String customerCode;
    @XmlElement(name = "ProductCode", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String productCode;
    @XmlElement(name = "ProductCodeQualifier")
    @XmlSchemaType(name = "token")
    protected ProductCodeQualifierType productCodeQualifier;
    @XmlElement(name = "ItemDescription", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String itemDescription;
    @XmlElement(name = "ItemQuantity", required = true)
    protected NumExp16Type itemQuantity;
    @XmlElement(name = "ItemUnitOfMeasurementCode", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String itemUnitOfMeasurementCode;
    @XmlElement(name = "UnitPrice", required = true)
    protected CurrencyAmountType unitPrice;
    @XmlElement(name = "ExtendedItemAmount", required = true)
    protected CurrencyAmountType extendedItemAmount;
    @XmlElement(name = "DiscountIndicator")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String discountIndicator;
    @XmlElement(name = "DiscountAmount")
    protected CurrencyAmountType discountAmount;
    @XmlElement(name = "ItemDiscountRate")
    protected NumExp16Type itemDiscountRate;
    @XmlElement(name = "CommodityCode")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String commodityCode;
    @XmlElement(name = "TotalTaxAmount")
    protected CurrencyAmountType totalTaxAmount;
    @XmlElement(name = "TypeOfSupply")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String typeOfSupply;
    @XmlElement(name = "TaxExemptIndicator")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String taxExemptIndicator;
    @XmlElement(name = "UniqueVATInvoiceReferenceNum", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String uniqueVATInvoiceReferenceNum;
    @XmlElement(name = "LineItemTotalAmount", required = true)
    protected CurrencyAmountType lineItemTotalAmount;

    /**
     * Gets the value of the financialRecordHeader property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialRecordHeaderType }
     *     
     */
    public FinancialRecordHeaderType getFinancialRecordHeader() {
        return financialRecordHeader;
    }

    /**
     * Sets the value of the financialRecordHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialRecordHeaderType }
     *     
     */
    public void setFinancialRecordHeader(FinancialRecordHeaderType value) {
        this.financialRecordHeader = value;
    }

    /**
     * Gets the value of the orderDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOrderDate() {
        return orderDate;
    }

    /**
     * Sets the value of the orderDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOrderDate(XMLGregorianCalendar value) {
        this.orderDate = value;
    }

    /**
     * Gets the value of the shipDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getShipDate() {
        return shipDate;
    }

    /**
     * Sets the value of the shipDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setShipDate(XMLGregorianCalendar value) {
        this.shipDate = value;
    }

    /**
     * Gets the value of the shipToHealthIndustryNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToHealthIndustryNum() {
        return shipToHealthIndustryNum;
    }

    /**
     * Sets the value of the shipToHealthIndustryNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToHealthIndustryNum(String value) {
        this.shipToHealthIndustryNum = value;
    }

    /**
     * Gets the value of the contractNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractNum() {
        return contractNum;
    }

    /**
     * Sets the value of the contractNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractNum(String value) {
        this.contractNum = value;
    }

    /**
     * Gets the value of the priceAdjustmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link PriceAdjustmentType }
     *     
     */
    public PriceAdjustmentType getPriceAdjustmentCode() {
        return priceAdjustmentCode;
    }

    /**
     * Sets the value of the priceAdjustmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceAdjustmentType }
     *     
     */
    public void setPriceAdjustmentCode(PriceAdjustmentType value) {
        this.priceAdjustmentCode = value;
    }

    /**
     * Gets the value of the poLineNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPOLineNum() {
        return poLineNum;
    }

    /**
     * Sets the value of the poLineNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPOLineNum(Integer value) {
        this.poLineNum = value;
    }

    /**
     * Gets the value of the specialCondition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialCondition() {
        return specialCondition;
    }

    /**
     * Sets the value of the specialCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialCondition(String value) {
        this.specialCondition = value;
    }

    /**
     * Gets the value of the generalText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralText() {
        return generalText;
    }

    /**
     * Sets the value of the generalText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralText(String value) {
        this.generalText = value;
    }

    /**
     * Gets the value of the productNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductNum() {
        return productNum;
    }

    /**
     * Sets the value of the productNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductNum(String value) {
        this.productNum = value;
    }

    /**
     * Gets the value of the healthIdNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthIdNum() {
        return healthIdNum;
    }

    /**
     * Sets the value of the healthIdNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthIdNum(String value) {
        this.healthIdNum = value;
    }

    /**
     * Gets the value of the glAccountingCode property.
     * 
     */
    public long getGLAccountingCode() {
        return glAccountingCode;
    }

    /**
     * Sets the value of the glAccountingCode property.
     * 
     */
    public void setGLAccountingCode(long value) {
        this.glAccountingCode = value;
    }

    /**
     * Gets the value of the productNumQualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductNumQualifier() {
        return productNumQualifier;
    }

    /**
     * Sets the value of the productNumQualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductNumQualifier(String value) {
        this.productNumQualifier = value;
    }

    /**
     * Gets the value of the dateOfPurchase property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfPurchase() {
        return dateOfPurchase;
    }

    /**
     * Sets the value of the dateOfPurchase property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfPurchase(XMLGregorianCalendar value) {
        this.dateOfPurchase = value;
    }

    /**
     * Gets the value of the customerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerCode() {
        return customerCode;
    }

    /**
     * Sets the value of the customerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerCode(String value) {
        this.customerCode = value;
    }

    /**
     * Gets the value of the productCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Sets the value of the productCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCode(String value) {
        this.productCode = value;
    }

    /**
     * Gets the value of the productCodeQualifier property.
     * 
     * @return
     *     possible object is
     *     {@link ProductCodeQualifierType }
     *     
     */
    public ProductCodeQualifierType getProductCodeQualifier() {
        return productCodeQualifier;
    }

    /**
     * Sets the value of the productCodeQualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductCodeQualifierType }
     *     
     */
    public void setProductCodeQualifier(ProductCodeQualifierType value) {
        this.productCodeQualifier = value;
    }

    /**
     * Gets the value of the itemDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemDescription() {
        return itemDescription;
    }

    /**
     * Sets the value of the itemDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemDescription(String value) {
        this.itemDescription = value;
    }

    /**
     * Gets the value of the itemQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link NumExp16Type }
     *     
     */
    public NumExp16Type getItemQuantity() {
        return itemQuantity;
    }

    /**
     * Sets the value of the itemQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumExp16Type }
     *     
     */
    public void setItemQuantity(NumExp16Type value) {
        this.itemQuantity = value;
    }

    /**
     * Gets the value of the itemUnitOfMeasurementCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemUnitOfMeasurementCode() {
        return itemUnitOfMeasurementCode;
    }

    /**
     * Sets the value of the itemUnitOfMeasurementCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemUnitOfMeasurementCode(String value) {
        this.itemUnitOfMeasurementCode = value;
    }

    /**
     * Gets the value of the unitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getUnitPrice() {
        return unitPrice;
    }

    /**
     * Sets the value of the unitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setUnitPrice(CurrencyAmountType value) {
        this.unitPrice = value;
    }

    /**
     * Gets the value of the extendedItemAmount property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getExtendedItemAmount() {
        return extendedItemAmount;
    }

    /**
     * Sets the value of the extendedItemAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setExtendedItemAmount(CurrencyAmountType value) {
        this.extendedItemAmount = value;
    }

    /**
     * Gets the value of the discountIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountIndicator() {
        return discountIndicator;
    }

    /**
     * Sets the value of the discountIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountIndicator(String value) {
        this.discountIndicator = value;
    }

    /**
     * Gets the value of the discountAmount property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Sets the value of the discountAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setDiscountAmount(CurrencyAmountType value) {
        this.discountAmount = value;
    }

    /**
     * Gets the value of the itemDiscountRate property.
     * 
     * @return
     *     possible object is
     *     {@link NumExp16Type }
     *     
     */
    public NumExp16Type getItemDiscountRate() {
        return itemDiscountRate;
    }

    /**
     * Sets the value of the itemDiscountRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumExp16Type }
     *     
     */
    public void setItemDiscountRate(NumExp16Type value) {
        this.itemDiscountRate = value;
    }

    /**
     * Gets the value of the commodityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommodityCode() {
        return commodityCode;
    }

    /**
     * Sets the value of the commodityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommodityCode(String value) {
        this.commodityCode = value;
    }

    /**
     * Gets the value of the totalTaxAmount property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getTotalTaxAmount() {
        return totalTaxAmount;
    }

    /**
     * Sets the value of the totalTaxAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setTotalTaxAmount(CurrencyAmountType value) {
        this.totalTaxAmount = value;
    }

    /**
     * Gets the value of the typeOfSupply property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeOfSupply() {
        return typeOfSupply;
    }

    /**
     * Sets the value of the typeOfSupply property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeOfSupply(String value) {
        this.typeOfSupply = value;
    }

    /**
     * Gets the value of the taxExemptIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxExemptIndicator() {
        return taxExemptIndicator;
    }

    /**
     * Sets the value of the taxExemptIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxExemptIndicator(String value) {
        this.taxExemptIndicator = value;
    }

    /**
     * Gets the value of the uniqueVATInvoiceReferenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueVATInvoiceReferenceNum() {
        return uniqueVATInvoiceReferenceNum;
    }

    /**
     * Sets the value of the uniqueVATInvoiceReferenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueVATInvoiceReferenceNum(String value) {
        this.uniqueVATInvoiceReferenceNum = value;
    }

    /**
     * Gets the value of the lineItemTotalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getLineItemTotalAmount() {
        return lineItemTotalAmount;
    }

    /**
     * Sets the value of the lineItemTotalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setLineItemTotalAmount(CurrencyAmountType value) {
        this.lineItemTotalAmount = value;
    }

}
