package cz.direct.finance.connector.deloitte.util;

import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;

class JulianConverter {

    static String toDate(LocalDate from, LocalDate to) {

        if (from == null || to == null) {
            return "00000";
        }

        StringBuilder sb = new StringBuilder(Long.toString(DAYS.between(from, to)));

        while (sb.length() < 5) {
            sb.insert(0, "0");
        }

        return sb.toString();
    }
}
