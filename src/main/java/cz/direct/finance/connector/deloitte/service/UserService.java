package cz.direct.finance.connector.deloitte.service;

import cz.direct.finance.connector.deloitte.model.Transaction;
import cz.direct.finance.connector.deloitte.model.User;
import cz.direct.finance.connector.deloitte.model.rest.GetUserResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class UserService {

    @Autowired
    private RestApiService restApiService;

    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    Map<String, User> getUsers(List<Transaction> transcationList) {
        Map<String, User> users = new HashMap<>();
        transcationList.forEach(transaction -> {
            for (String userId : transaction.getUserIdList()) {
                if (!users.containsKey(userId)) {
                    users.put(userId, this.getUserById(userId, 0));
                }
            }
        });
        return users;
    }

    private User getUserById(String userId, int attempt) {
        ResponseEntity<GetUserResponse> response;
        try {
            response = restApiService.getUser(userId);
            if (response != null) {
                return new User(userId, response.getBody().getFirstName(), response.getBody().getLastName());
            } else {
                throw new RuntimeException("Empty response body!"); // TODO should I throw exception here or continue processing with user as null?
            }
        } catch (ResourceAccessException e) {
            if (attempt < 3) {
                log.warn("Connection timeout. Attempt no " + (attempt + 1));
                return getUserById(userId, ++attempt);
            } else {
                log.error("Connection timeout.", e);
                throw e;
            }
        } catch (RestClientException e) {
            log.error("error during receiving card " + userId + ". Message: "
                    + (e instanceof HttpClientErrorException ? ((HttpClientErrorException) e).getResponseBodyAsString() : e.getMessage()), e);
            throw new RuntimeException(e); // TODO should I throw exception here or continue processing with user as null?
        }
    }
}
