package cz.direct.finance.connector.deloitte.model.rest;

public class GetUserRequest {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public GetUserRequest(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "GetUserRequest{" +
                "userId='" + userId + '\'' +
                '}';
    }
}
