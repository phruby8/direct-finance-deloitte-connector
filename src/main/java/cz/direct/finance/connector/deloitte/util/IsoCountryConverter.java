package cz.direct.finance.connector.deloitte.util;

import java.util.Map;
import java.util.TreeMap;

class IsoCountryConverter {

    private static Map<String, String> map = new TreeMap<>();

    static {
        map.put("AFG", "004");
        map.put("ALA", "248");
        map.put("ALB", "008");
        map.put("DZA", "012");
        map.put("ASM", "016");
        map.put("VIR", "850");
        map.put("AND", "020");
        map.put("AGO", "024");
        map.put("AIA", "660");
        map.put("ATA", "010");
        map.put("ATG", "028");
        map.put("ARG", "032");
        map.put("ARM", "051");
        map.put("ABW", "533");
        map.put("AUS", "036");
        map.put("AZE", "031");
        map.put("BHS", "044");
        map.put("BHR", "048");
        map.put("BGD", "050");
        map.put("BRB", "052");
        map.put("BEL", "056");
        map.put("BLZ", "084");
        map.put("BLR", "112");
        map.put("BEN", "204");
        map.put("BMU", "060");
        map.put("BTN", "064");
        map.put("BOL", "068");
        map.put("BES", "535");
        map.put("BIH", "070");
        map.put("BWA", "072");
        map.put("BVT", "074");
        map.put("BRA", "076");
        map.put("IOT", "086");
        map.put("VGB", "092");
        map.put("BRN", "096");
        map.put("BGR", "100");
        map.put("BFA", "854");
        map.put("BDI", "108");
        map.put("COK", "184");
        map.put("CUW", "531");
        map.put("TCD", "148");
        map.put("MNE", "499");
        map.put("CZE", "203");
        map.put("CHN", "156");
        map.put("DNK", "208");
        map.put("COD", "180");
        map.put("DMA", "212");
        map.put("DOM", "214");
        map.put("DJI", "262");
        map.put("EGY", "818");
        map.put("ECU", "218");
        map.put("ERI", "232");
        map.put("EST", "233");
        map.put("ETH", "231");
        map.put("FRO", "234");
        map.put("FLK", "238");
        map.put("FJI", "242");
        map.put("PHL", "608");
        map.put("FIN", "246");
        map.put("FRA", "250");
        map.put("GUF", "254");
        map.put("ATF", "260");
        map.put("PYF", "258");
        map.put("GAB", "266");
        map.put("GMB", "270");
        map.put("GHA", "288");
        map.put("GIB", "292");
        map.put("GRD", "308");
        map.put("GRL", "304");
        map.put("GEO", "268");
        map.put("GLP", "312");
        map.put("GUM", "316");
        map.put("GTM", "320");
        map.put("GIN", "324");
        map.put("GNB", "624");
        map.put("GGY", "831");
        map.put("GUY", "328");
        map.put("HTI", "332");
        map.put("HMD", "334");
        map.put("HND", "340");
        map.put("HKG", "344");
        map.put("CHL", "152");
        map.put("HRV", "191");
        map.put("IND", "356");
        map.put("IDN", "360");
        map.put("IRQ", "368");
        map.put("IRN", "364");
        map.put("IRL", "372");
        map.put("ISL", "352");
        map.put("ITA", "380");
        map.put("ISR", "376");
        map.put("JAM", "388");
        map.put("JPN", "392");
        map.put("YEM", "887");
        map.put("JEY", "832");
        map.put("ZAF", "710");
        map.put("SGS", "239");
        map.put("KOR", "410");
        map.put("SSD", "728");
        map.put("JOR", "400");
        map.put("CYM", "136");
        map.put("KHM", "116");
        map.put("CMR", "120");
        map.put("CAN", "124");
        map.put("CPV", "132");
        map.put("QAT", "634");
        map.put("KAZ", "398");
        map.put("KEN", "404");
        map.put("KIR", "296");
        map.put("CCK", "166");
        map.put("COL", "170");
        map.put("COM", "174");
        map.put("COG", "178");
        map.put("CRI", "188");
        map.put("CUB", "192");
        map.put("KWT", "414");
        map.put("CYP", "196");
        map.put("KGZ", "417");
        map.put("LAO", "418");
        map.put("LSO", "426");
        map.put("LBN", "422");
        map.put("LBR", "430");
        map.put("LBY", "434");
        map.put("LIE", "438");
        map.put("LTU", "440");
        map.put("LVA", "428");
        map.put("LUX", "442");
        map.put("MAC", "446");
        map.put("MDG", "450");
        map.put("HUN", "348");
        map.put("MKD", "807");
        map.put("MYS", "458");
        map.put("MWI", "454");
        map.put("MDV", "462");
        map.put("MLI", "466");
        map.put("MLT", "470");
        map.put("IMN", "833");
        map.put("MAR", "504");
        map.put("MHL", "584");
        map.put("MTQ", "474");
        map.put("MUS", "480");
        map.put("MRT", "478");
        map.put("MYT", "175");
        map.put("UMI", "581");
        map.put("MEX", "484");
        map.put("FSM", "583");
        map.put("MDA", "498");
        map.put("MCO", "492");
        map.put("MNG", "496");
        map.put("MSR", "500");
        map.put("MOZ", "508");
        map.put("MMR", "104");
        map.put("NAM", "516");
        map.put("NRU", "520");
        map.put("DEU", "276");
        map.put("NPL", "524");
        map.put("NER", "562");
        map.put("NGA", "566");
        map.put("NIC", "558");
        map.put("NIU", "570");
        map.put("NLD", "528");
        map.put("NFK", "574");
        map.put("NOR", "578");
        map.put("NCL", "540");
        map.put("NZL", "554");
        map.put("OMN", "512");
        map.put("PAK", "586");
        map.put("PLW", "585");
        map.put("PSE", "275");
        map.put("PAN", "591");
        map.put("PNG", "598");
        map.put("PRY", "600");
        map.put("PER", "604");
        map.put("PCN", "612");
        map.put("CIV", "384");
        map.put("POL", "616");
        map.put("PRI", "630");
        map.put("PRT", "620");
        map.put("AUT", "040");
        map.put("REU", "638");
        map.put("GNQ", "226");
        map.put("ROU", "642");
        map.put("RUS", "643");
        map.put("RWA", "646");
        map.put("GRC", "300");
        map.put("SPM", "666");
        map.put("SLV", "222");
        map.put("WSM", "882");
        map.put("SMR", "674");
        map.put("SAU", "682");
        map.put("SEN", "686");
        map.put("PRK", "408");
        map.put("MNP", "580");
        map.put("SYC", "690");
        map.put("SLE", "694");
        map.put("SGP", "702");
        map.put("SVK", "703");
        map.put("SVN", "705");
        map.put("SOM", "706");
        map.put("ARE", "784");
        map.put("GBR", "826");
        map.put("USA", "840");
        map.put("SRB", "688");
        map.put("LKA", "144");
        map.put("CAF", "140");
        map.put("SDN", "729");
        map.put("SUR", "740");
        map.put("SHN", "654");
        map.put("LCA", "662");
        map.put("BLM", "652");
        map.put("KNA", "659");
        map.put("MAF", "663");
        map.put("SXM", "534");
        map.put("STP", "678");
        map.put("VCT", "670");
        map.put("SWZ", "748");
        map.put("SYR", "760");
        map.put("SLB", "090");
        map.put("ESP", "724");
        map.put("SJM", "744");
        map.put("SWE", "752");
        map.put("CHE", "756");
        map.put("TJK", "762");
        map.put("TZA", "834");
        map.put("THA", "764");
        map.put("TWN", "158");
        map.put("TGO", "768");
        map.put("TKL", "772");
        map.put("TON", "776");
        map.put("TTO", "780");
        map.put("TUN", "788");
        map.put("TUR", "792");
        map.put("TKM", "795");
        map.put("TCA", "796");
        map.put("TUV", "798");
        map.put("UGA", "800");
        map.put("UKR", "804");
        map.put("URY", "858");
        map.put("UZB", "860");
        map.put("CXR", "162");
        map.put("VUT", "548");
        map.put("VAT", "336");
        map.put("VEN", "862");
        map.put("VNM", "704");
        map.put("TLS", "626");
        map.put("WLF", "876");
        map.put("ZMB", "894");
        map.put("ESH", "732");
        map.put("ZWE", "716");
    }

    static String getIsoCountryCode(String code) {
        if (code == null) return "203";

        if (map.containsKey(code.toUpperCase()))
            return map.get(code.toUpperCase());
        else
            return "203";
    }
}




