package cz.direct.finance.connector.deloitte.service;

import cz.direct.finance.connector.deloitte.config.ConfigurationProperties;
import cz.direct.finance.connector.deloitte.model.DataToParse;
import cz.direct.finance.connector.deloitte.util.IssuerEntityCreator;
import cz.direct.finance.connector.deloitte.util.HeaderCreator;
import cz.direct.finance.connector.deloitte.xsd.CDFTransmissionFileEntityType;
import cz.direct.finance.connector.deloitte.xsd.TransRecordHeaderType;
import cz.direct.finance.connector.deloitte.xsd.TransmissionTrailer9999Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class XmlParsingService {

    private static final Logger log = LoggerFactory.getLogger(XmlParsingService.class);

    @Autowired
    private ConfigurationProperties configurationProperties;

    CDFTransmissionFileEntityType setData(DataToParse dataToParse) {
        // create root element
        CDFTransmissionFileEntityType cdfTransmissionFileEntityType = new CDFTransmissionFileEntityType();

        // add headers
        cdfTransmissionFileEntityType.setTransmissionHeader1000(HeaderCreator.createTransmissionHeader());

        // parse other data to the root element
        log.info("Starts parsing data to classes generated from xsd schema.");
        dataToParse.getTransactions().forEach(transaction -> {
            try {
                cdfTransmissionFileEntityType.getIssuerEntity().add(IssuerEntityCreator.create(transaction, dataToParse, configurationProperties.getCompanyUid()));
            } catch (ControlledSkipException e) {
                // empty block - want to continue next iteration
            } catch (Exception e) {
                log.info("Parsing transaction failed. transaction id: " + transaction.getId(), e);
                throw new RuntimeException("Parsing transaction failed. transaction id: " + transaction.getId(), e);
            }
        });


        TransmissionTrailer9999Type transmissionTrailer9999Type = new TransmissionTrailer9999Type();
        TransRecordHeaderType transRecordHeaderType = new TransRecordHeaderType();
        transRecordHeaderType.setSequenceNum(1);
        transmissionTrailer9999Type.setTransRecordHeader(transRecordHeaderType);
        cdfTransmissionFileEntityType.setTransmissionTrailer9999(transmissionTrailer9999Type);

        return cdfTransmissionFileEntityType;
    }
}
