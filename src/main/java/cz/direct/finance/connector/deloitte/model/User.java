package cz.direct.finance.connector.deloitte.model;

public class User {

    private String cardId;
    private String firstName;
    private String lastName;

    public String getCardId() {
        return cardId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public User(String cardId, String firstName, String lastName) {
        this.cardId = cardId;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
