package cz.direct.finance.connector.deloitte.service;

import cz.direct.finance.connector.deloitte.config.ConfigurationProperties;
import cz.direct.finance.connector.deloitte.model.DataToParse;
import cz.direct.finance.connector.deloitte.util.XmlValidator;
import cz.direct.finance.connector.deloitte.xsd.CDFTransmissionFileEntityType;
import cz.direct.finance.connector.deloitte.xsd.ObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.time.LocalDate;

@Service
public class FileService {

    private static final Logger log = LoggerFactory.getLogger(FileService.class);

    @Autowired
    private ConfigurationProperties configurationProperties;

    @Autowired
    private XmlParsingService xmlParsingService;

    // creates filename with timeStamp
    @SuppressWarnings("Duplicates")
    String getFileName() {
        LocalDate yesterday = LocalDate.now().minusDays(1);
        try {
            String[] parts = configurationProperties.getFileName().split("\\.");
            return configurationProperties.getPath() + File.separator + parts[0] + "_" + yesterday + "." + parts[1];
        } catch (Exception e) {
            log.warn("Failed to create name based on configuration. Default name will be used", e);
            return configurationProperties.getPath() + File.separator + "defaultOutput_" + yesterday + ".xml";
        }
    }

    // creates filename with timeStamp
    @SuppressWarnings("Duplicates")
    String getDeleteFileName() {
        LocalDate dayBeforeYesterday = LocalDate.now().minusDays(2);
        try {
            String[] parts = configurationProperties.getFileName().split("\\.");
            return configurationProperties.getPath() + File.separator + parts[0] + "_" + dayBeforeYesterday + "." + parts[1];
        } catch (Exception e) {
            log.warn("Failed to create name based on configuration. Default name will be used", e);
            return configurationProperties.getPath() + File.separator + "defaultOutput_" + dayBeforeYesterday + ".xml";
        }
    }

    String getBackupFileName () {
        LocalDate yesterday = LocalDate.now().minusDays(1);
        return configurationProperties.getBackup() + File.separator + "backup_" + yesterday + ".xml";
    }

    void createXmlFile(DataToParse dataToParse, String fileName) throws JAXBException {

        File file = new File(fileName);
        try {
            // parse Data
            CDFTransmissionFileEntityType cdfTransmissionFileEntityType = xmlParsingService.setData(dataToParse);

            // create marshaller
            log.info("Starts creating JAXBContext and Marshaller");
            JAXBContext context = JAXBContext.newInstance(CDFTransmissionFileEntityType.class);
            ObjectFactory objectFactory = new ObjectFactory();
            JAXBElement<CDFTransmissionFileEntityType> element = objectFactory.createCDFTransmissionFile(cdfTransmissionFileEntityType);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // create file
            log.info("Starts creating Xml File: " + fileName);
            marshaller.marshal(element, file);

            // validate agains xsd
            XmlValidator.validateAgainstXSD(file, configurationProperties.getSchemaPath());
            log.info("File succesfully" + fileName + " created.");

            // deletes old file
            deleteOldFile();
        } catch (Exception e) {
            // deletes file if there was an exception
            if (!file.delete()) {
                log.error("Could not delete the file");
            }
            throw e;
        }
    }

    void deleteOldFile() {
        File fileToDelete = new File(getDeleteFileName());
        if(!fileToDelete.exists()) {
            throw new RuntimeException("File does not exists");
        }
        if (!fileToDelete.delete()) {
            throw new RuntimeException("File cannot be deleted");
        }
    }

}
