package cz.direct.finance.connector.deloitte.model;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class Transaction {
    private String id;
    private String cardId;
    private String cardEmbossName;
    private String cardMaskedNumber;
    private LocalDate transactionDate;
    private LocalDate settlementDate;
    private Double originalAmount;
    private String originalCurrency;
    private Double signedAmount;
    private Double privateSpendingAmount;
    private Double serviceFee;
    private String transactionStatus;
    private String transactionType;
    private String merchantName;
    private String merchantLocation;
    private String merchantCountry;
    private String categoryName;
    private String primaryCostCenter;
    private String secondaryCostCenter;
    private String transactionNote;
    private String receiptId;
    private String cardType;
    private String[] userIdList;
    private String mcc;

    public String getId() {
        return id;
    }

    public String getCardId() {
        return cardId;
    }

    public String getCardEmbossName() {
        return cardEmbossName;
    }

    public String getCardMaskedNumber() {
        return cardMaskedNumber;
    }

    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    public LocalDate getSettlementDate() {
        return settlementDate;
    }

    public Double getOriginalAmount() {
        return originalAmount;
    }

    public String getOriginalCurrency() {
        return originalCurrency;
    }

    public Double getSignedAmount() {
        return signedAmount;
    }

    public Double getPrivateSpendingAmount() {
        return privateSpendingAmount;
    }

    public Double getServiceFee() {
        return serviceFee;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public String getMerchantLocation() {
        return merchantLocation;
    }

    public String getMerchantCountry() {
        return merchantCountry;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getPrimaryCostCenter() {
        return primaryCostCenter;
    }

    public String getSecondaryCostCenter() {
        return secondaryCostCenter;
    }

    public String getTransactionNote() {
        return transactionNote;
    }

    public String getReceiptId() {
        return receiptId;
    }

    public String getCardType() {
        return cardType;
    }

    public String[] getUserIdList() {
        return userIdList;
    }

    public String getMcc() {
        return mcc;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id='" + id + '\'' +
                ", cardId='" + cardId + '\'' +
                ", cardEmbossName='" + cardEmbossName + '\'' +
                ", cardMaskedNumber='" + cardMaskedNumber + '\'' +
                ", transactionDate=" + transactionDate +
                ", settlementDate=" + settlementDate +
                ", originalAmount=" + originalAmount +
                ", originalCurrency='" + originalCurrency + '\'' +
                ", signedAmount=" + signedAmount +
                ", privateSpendingAmount=" + privateSpendingAmount +
                ", serviceFee=" + serviceFee +
                ", transactionStatus='" + transactionStatus + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", merchantName='" + merchantName + '\'' +
                ", merchantLocation='" + merchantLocation + '\'' +
                ", merchantCountry='" + merchantCountry + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", primaryCostCenter='" + primaryCostCenter + '\'' +
                ", secondaryCostCenter='" + secondaryCostCenter + '\'' +
                ", transactionNote='" + transactionNote + '\'' +
                ", receiptId='" + receiptId + '\'' +
                ", cardType='" + cardType + '\'' +
                ", userIdList=" + Arrays.toString(userIdList) +
                ", mcc='" + mcc + '\'' +
                '}';
    }
}
