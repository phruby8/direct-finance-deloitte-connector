package cz.direct.finance.connector.deloitte.service;

import cz.direct.finance.connector.deloitte.model.CardResponse;
import cz.direct.finance.connector.deloitte.model.rest.GetCardsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CardService {

    @Autowired
    private RestApiService restApiService;

    private static final Logger log = LoggerFactory.getLogger(CardService.class);

    Map<String, CardResponse> getCards(int attempt) {

        Map<String, CardResponse> map = new HashMap<>();
        try {
            ResponseEntity<GetCardsResponse> cardsResponse = restApiService.getCards();
            if (cardsResponse.getBody().getCardList().size() > 0) {
                map = cardsResponse.getBody().getCardList().stream().collect(Collectors.toMap(CardResponse::getCardId, cardResponse -> cardResponse));
            }
        } catch (ResourceAccessException e) {
            if (attempt < 3) {
                log.warn("Connection timeout. Attempt no " + (attempt + 1));
                return getCards(++attempt);
            } else {
                log.error("Connection timeout.", e);
                throw e;
            }
        } catch (RestClientException e) {
            log.error("error during receiving Cards. Message: "
                    + (e instanceof HttpClientErrorException ? ((HttpClientErrorException) e).getResponseBodyAsString() : e.getMessage()), e);
            throw e;
        }
        return map;
    }
}
