package cz.direct.finance.connector.deloitte.model.rest;

public class GetCardsRequest {

    // I was unable to call with empty body although the empty response body expected
    private String test;

    public GetCardsRequest(String test) {
        this.test = test;
    }

    public String getTest() {
        return test;
    }
}
