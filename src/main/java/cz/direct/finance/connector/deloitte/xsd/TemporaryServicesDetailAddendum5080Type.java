//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.11.20 at 09:18:30 AM CET 
//


package cz.direct.finance.connector.deloitte.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TemporaryServicesDetailAddendum_5080Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TemporaryServicesDetailAddendum_5080Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="FinancialRecordHeader" type="{}FinancialRecordHeaderType"/>
 *         &lt;element name="EmployeeTempNameId" type="{}Char40Type" minOccurs="0"/>
 *         &lt;element name="EINOrId" type="{}Char30Type" minOccurs="0"/>
 *         &lt;element name="JobDescription" type="{}Char40Type" minOccurs="0"/>
 *         &lt;element name="JobCode" type="{}Char20Type" minOccurs="0"/>
 *         &lt;element name="FlatRateIndicator" type="{}TrueFalseType" minOccurs="0"/>
 *         &lt;element name="RegularHoursWorked" type="{}NumExp16Type" minOccurs="0"/>
 *         &lt;element name="RegularHoursRate" type="{}CurrencyAmountType" minOccurs="0"/>
 *         &lt;element name="OvertimeHoursWorked" type="{}NumExp16Type" minOccurs="0"/>
 *         &lt;element name="OvertimeHoursRate" type="{}CurrencyAmountType" minOccurs="0"/>
 *         &lt;element name="TempStartDate" type="{}MasterCardDateType" minOccurs="0"/>
 *         &lt;element name="TempWeekEnding" type="{}MasterCardDateType" minOccurs="0"/>
 *         &lt;element name="RequesterNameOrId" type="{}Char40Type" minOccurs="0"/>
 *         &lt;element name="SupervisorReportsTo" type="{}Char40Type" minOccurs="0"/>
 *         &lt;element name="TimeSheetNum" type="{}Char20Type" minOccurs="0"/>
 *         &lt;element name="DiscountIndicator" type="{}TrueFalseUnknownType" minOccurs="0"/>
 *         &lt;element name="DiscountAmount" type="{}CurrencyAmountType" minOccurs="0"/>
 *         &lt;element name="SubTotalAmount" type="{}CurrencyAmountType" minOccurs="0"/>
 *         &lt;element name="MiscellaneousExpense" type="{}MiscellaneousExpenseType" minOccurs="0"/>
 *         &lt;element name="MiscellaneousExpense2" type="{}MiscellaneousExpenseType" minOccurs="0"/>
 *         &lt;element name="MiscellaneousExpense3" type="{}MiscellaneousExpenseType" minOccurs="0"/>
 *         &lt;element name="MiscellaneousExpense4" type="{}MiscellaneousExpenseType" minOccurs="0"/>
 *         &lt;element name="CommodityCode" type="{}Char15Type" minOccurs="0"/>
 *         &lt;element name="TaxExemptIndicator" type="{}SimpleCodeType" minOccurs="0"/>
 *         &lt;element name="CustomerCode" type="{}Char49Type" minOccurs="0"/>
 *         &lt;element name="TotalTaxAmount" type="{}CurrencyAmountType" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TemporaryServicesDetailAddendum_5080Type", propOrder = {

})
public class TemporaryServicesDetailAddendum5080Type {

    @XmlElement(name = "FinancialRecordHeader", required = true)
    protected FinancialRecordHeaderType financialRecordHeader;
    @XmlElement(name = "EmployeeTempNameId")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String employeeTempNameId;
    @XmlElement(name = "EINOrId")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String einOrId;
    @XmlElement(name = "JobDescription")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String jobDescription;
    @XmlElement(name = "JobCode")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String jobCode;
    @XmlElement(name = "FlatRateIndicator")
    @XmlSchemaType(name = "token")
    protected TrueFalseType flatRateIndicator;
    @XmlElement(name = "RegularHoursWorked")
    protected NumExp16Type regularHoursWorked;
    @XmlElement(name = "RegularHoursRate")
    protected CurrencyAmountType regularHoursRate;
    @XmlElement(name = "OvertimeHoursWorked")
    protected NumExp16Type overtimeHoursWorked;
    @XmlElement(name = "OvertimeHoursRate")
    protected CurrencyAmountType overtimeHoursRate;
    @XmlElement(name = "TempStartDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar tempStartDate;
    @XmlElement(name = "TempWeekEnding")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar tempWeekEnding;
    @XmlElement(name = "RequesterNameOrId")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String requesterNameOrId;
    @XmlElement(name = "SupervisorReportsTo")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String supervisorReportsTo;
    @XmlElement(name = "TimeSheetNum")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String timeSheetNum;
    @XmlElement(name = "DiscountIndicator")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String discountIndicator;
    @XmlElement(name = "DiscountAmount")
    protected CurrencyAmountType discountAmount;
    @XmlElement(name = "SubTotalAmount")
    protected CurrencyAmountType subTotalAmount;
    @XmlElement(name = "MiscellaneousExpense")
    protected MiscellaneousExpenseType miscellaneousExpense;
    @XmlElement(name = "MiscellaneousExpense2")
    protected MiscellaneousExpenseType miscellaneousExpense2;
    @XmlElement(name = "MiscellaneousExpense3")
    protected MiscellaneousExpenseType miscellaneousExpense3;
    @XmlElement(name = "MiscellaneousExpense4")
    protected MiscellaneousExpenseType miscellaneousExpense4;
    @XmlElement(name = "CommodityCode")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String commodityCode;
    @XmlElement(name = "TaxExemptIndicator")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String taxExemptIndicator;
    @XmlElement(name = "CustomerCode")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String customerCode;
    @XmlElement(name = "TotalTaxAmount")
    protected CurrencyAmountType totalTaxAmount;

    /**
     * Gets the value of the financialRecordHeader property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialRecordHeaderType }
     *     
     */
    public FinancialRecordHeaderType getFinancialRecordHeader() {
        return financialRecordHeader;
    }

    /**
     * Sets the value of the financialRecordHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialRecordHeaderType }
     *     
     */
    public void setFinancialRecordHeader(FinancialRecordHeaderType value) {
        this.financialRecordHeader = value;
    }

    /**
     * Gets the value of the employeeTempNameId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeTempNameId() {
        return employeeTempNameId;
    }

    /**
     * Sets the value of the employeeTempNameId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeTempNameId(String value) {
        this.employeeTempNameId = value;
    }

    /**
     * Gets the value of the einOrId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEINOrId() {
        return einOrId;
    }

    /**
     * Sets the value of the einOrId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEINOrId(String value) {
        this.einOrId = value;
    }

    /**
     * Gets the value of the jobDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobDescription() {
        return jobDescription;
    }

    /**
     * Sets the value of the jobDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobDescription(String value) {
        this.jobDescription = value;
    }

    /**
     * Gets the value of the jobCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobCode() {
        return jobCode;
    }

    /**
     * Sets the value of the jobCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobCode(String value) {
        this.jobCode = value;
    }

    /**
     * Gets the value of the flatRateIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link TrueFalseType }
     *     
     */
    public TrueFalseType getFlatRateIndicator() {
        return flatRateIndicator;
    }

    /**
     * Sets the value of the flatRateIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrueFalseType }
     *     
     */
    public void setFlatRateIndicator(TrueFalseType value) {
        this.flatRateIndicator = value;
    }

    /**
     * Gets the value of the regularHoursWorked property.
     * 
     * @return
     *     possible object is
     *     {@link NumExp16Type }
     *     
     */
    public NumExp16Type getRegularHoursWorked() {
        return regularHoursWorked;
    }

    /**
     * Sets the value of the regularHoursWorked property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumExp16Type }
     *     
     */
    public void setRegularHoursWorked(NumExp16Type value) {
        this.regularHoursWorked = value;
    }

    /**
     * Gets the value of the regularHoursRate property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getRegularHoursRate() {
        return regularHoursRate;
    }

    /**
     * Sets the value of the regularHoursRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setRegularHoursRate(CurrencyAmountType value) {
        this.regularHoursRate = value;
    }

    /**
     * Gets the value of the overtimeHoursWorked property.
     * 
     * @return
     *     possible object is
     *     {@link NumExp16Type }
     *     
     */
    public NumExp16Type getOvertimeHoursWorked() {
        return overtimeHoursWorked;
    }

    /**
     * Sets the value of the overtimeHoursWorked property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumExp16Type }
     *     
     */
    public void setOvertimeHoursWorked(NumExp16Type value) {
        this.overtimeHoursWorked = value;
    }

    /**
     * Gets the value of the overtimeHoursRate property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getOvertimeHoursRate() {
        return overtimeHoursRate;
    }

    /**
     * Sets the value of the overtimeHoursRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setOvertimeHoursRate(CurrencyAmountType value) {
        this.overtimeHoursRate = value;
    }

    /**
     * Gets the value of the tempStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTempStartDate() {
        return tempStartDate;
    }

    /**
     * Sets the value of the tempStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTempStartDate(XMLGregorianCalendar value) {
        this.tempStartDate = value;
    }

    /**
     * Gets the value of the tempWeekEnding property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTempWeekEnding() {
        return tempWeekEnding;
    }

    /**
     * Sets the value of the tempWeekEnding property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTempWeekEnding(XMLGregorianCalendar value) {
        this.tempWeekEnding = value;
    }

    /**
     * Gets the value of the requesterNameOrId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequesterNameOrId() {
        return requesterNameOrId;
    }

    /**
     * Sets the value of the requesterNameOrId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequesterNameOrId(String value) {
        this.requesterNameOrId = value;
    }

    /**
     * Gets the value of the supervisorReportsTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupervisorReportsTo() {
        return supervisorReportsTo;
    }

    /**
     * Sets the value of the supervisorReportsTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupervisorReportsTo(String value) {
        this.supervisorReportsTo = value;
    }

    /**
     * Gets the value of the timeSheetNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeSheetNum() {
        return timeSheetNum;
    }

    /**
     * Sets the value of the timeSheetNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeSheetNum(String value) {
        this.timeSheetNum = value;
    }

    /**
     * Gets the value of the discountIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountIndicator() {
        return discountIndicator;
    }

    /**
     * Sets the value of the discountIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountIndicator(String value) {
        this.discountIndicator = value;
    }

    /**
     * Gets the value of the discountAmount property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Sets the value of the discountAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setDiscountAmount(CurrencyAmountType value) {
        this.discountAmount = value;
    }

    /**
     * Gets the value of the subTotalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getSubTotalAmount() {
        return subTotalAmount;
    }

    /**
     * Sets the value of the subTotalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setSubTotalAmount(CurrencyAmountType value) {
        this.subTotalAmount = value;
    }

    /**
     * Gets the value of the miscellaneousExpense property.
     * 
     * @return
     *     possible object is
     *     {@link MiscellaneousExpenseType }
     *     
     */
    public MiscellaneousExpenseType getMiscellaneousExpense() {
        return miscellaneousExpense;
    }

    /**
     * Sets the value of the miscellaneousExpense property.
     * 
     * @param value
     *     allowed object is
     *     {@link MiscellaneousExpenseType }
     *     
     */
    public void setMiscellaneousExpense(MiscellaneousExpenseType value) {
        this.miscellaneousExpense = value;
    }

    /**
     * Gets the value of the miscellaneousExpense2 property.
     * 
     * @return
     *     possible object is
     *     {@link MiscellaneousExpenseType }
     *     
     */
    public MiscellaneousExpenseType getMiscellaneousExpense2() {
        return miscellaneousExpense2;
    }

    /**
     * Sets the value of the miscellaneousExpense2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link MiscellaneousExpenseType }
     *     
     */
    public void setMiscellaneousExpense2(MiscellaneousExpenseType value) {
        this.miscellaneousExpense2 = value;
    }

    /**
     * Gets the value of the miscellaneousExpense3 property.
     * 
     * @return
     *     possible object is
     *     {@link MiscellaneousExpenseType }
     *     
     */
    public MiscellaneousExpenseType getMiscellaneousExpense3() {
        return miscellaneousExpense3;
    }

    /**
     * Sets the value of the miscellaneousExpense3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link MiscellaneousExpenseType }
     *     
     */
    public void setMiscellaneousExpense3(MiscellaneousExpenseType value) {
        this.miscellaneousExpense3 = value;
    }

    /**
     * Gets the value of the miscellaneousExpense4 property.
     * 
     * @return
     *     possible object is
     *     {@link MiscellaneousExpenseType }
     *     
     */
    public MiscellaneousExpenseType getMiscellaneousExpense4() {
        return miscellaneousExpense4;
    }

    /**
     * Sets the value of the miscellaneousExpense4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link MiscellaneousExpenseType }
     *     
     */
    public void setMiscellaneousExpense4(MiscellaneousExpenseType value) {
        this.miscellaneousExpense4 = value;
    }

    /**
     * Gets the value of the commodityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommodityCode() {
        return commodityCode;
    }

    /**
     * Sets the value of the commodityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommodityCode(String value) {
        this.commodityCode = value;
    }

    /**
     * Gets the value of the taxExemptIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxExemptIndicator() {
        return taxExemptIndicator;
    }

    /**
     * Sets the value of the taxExemptIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxExemptIndicator(String value) {
        this.taxExemptIndicator = value;
    }

    /**
     * Gets the value of the customerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerCode() {
        return customerCode;
    }

    /**
     * Sets the value of the customerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerCode(String value) {
        this.customerCode = value;
    }

    /**
     * Gets the value of the totalTaxAmount property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getTotalTaxAmount() {
        return totalTaxAmount;
    }

    /**
     * Sets the value of the totalTaxAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setTotalTaxAmount(CurrencyAmountType value) {
        this.totalTaxAmount = value;
    }

}
