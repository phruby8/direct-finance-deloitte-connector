package cz.direct.finance.connector.deloitte.model.rest;


public class GetUserResponse {
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String employeeNumber;
    private String userState;
    private String kycStatus;
    private Boolean usesExpenseManagement;
    private Boolean usesApplication;
    private String language;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public String getUserState() {
        return userState;
    }

    public String getKycStatus() {
        return kycStatus;
    }

    public Boolean getUsesExpenseManagement() {
        return usesExpenseManagement;
    }

    public Boolean getUsesApplication() {
        return usesApplication;
    }

    public String getLanguage() {
        return language;
    }

    @Override
    public String toString() {
        return "GetUserResponse{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", employeeNumber='" + employeeNumber + '\'' +
                ", userState='" + userState + '\'' +
                ", kycStatus='" + kycStatus + '\'' +
                ", usesExpenseManagement=" + usesExpenseManagement +
                ", usesApplication=" + usesApplication +
                ", language='" + language + '\'' +
                '}';
    }
}
