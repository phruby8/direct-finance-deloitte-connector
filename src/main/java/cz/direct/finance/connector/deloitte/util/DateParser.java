package cz.direct.finance.connector.deloitte.util;

import cz.direct.finance.connector.deloitte.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;


class DateParser {

    private static final Logger log = LoggerFactory.getLogger(DateParser.class);

    static XMLGregorianCalendar toXMLGregCal(String value) {
        if (value == null) {
            return null;
        }
        try {
            XMLGregorianCalendar result = DatatypeFactory.newInstance().newXMLGregorianCalendar(value);
            result.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
            return result;
        } catch (DatatypeConfigurationException e) {
            log.error("Failed to parse String to XMLGregorianCalendar. Value tried to convert: " + value, e);
            return null;
        }
    }

    static XMLGregorianCalendar toXMLGregCal(LocalDate value) {
        if (value == null) {
            return null;
        }
        return toXMLGregCal(value.toString());
    }
}
