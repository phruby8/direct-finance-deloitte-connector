package cz.direct.finance.connector.deloitte;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
public class DeloitteApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeloitteApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void afterStart() {
        System.out.println("STARTED");
    }
}
