//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.11.20 at 09:18:30 AM CET 
//


package cz.direct.finance.connector.deloitte.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for AddressEntityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressEntityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="LocaleCode" type="{}LanguageLocaleType"/>
 *         &lt;element name="PrimaryAddressIndicator" type="{}TrueFalseType"/>
 *         &lt;element name="DBAddressLine" type="{}Char64Type"/>
 *         &lt;element name="DBAddressLine2" type="{}Char64Type" minOccurs="0"/>
 *         &lt;element name="DBCity" type="{}Char50Type"/>
 *         &lt;element name="DBStateProvince" type="{}Char10Type" minOccurs="0"/>
 *         &lt;element name="DBCountryCode" type="{}DBCountryCodeType"/>
 *         &lt;element name="DBPostalCode" type="{}DBPostalCodeType"/>
 *         &lt;element name="DBCountry" type="{}Char50Type" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressEntityType", propOrder = {

})
public class AddressEntityType {

    @XmlElement(name = "LocaleCode", required = true, defaultValue = "ENU")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String localeCode;
    @XmlElement(name = "PrimaryAddressIndicator", required = true)
    @XmlSchemaType(name = "token")
    protected TrueFalseType primaryAddressIndicator;
    @XmlElement(name = "DBAddressLine", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String dbAddressLine;
    @XmlElement(name = "DBAddressLine2")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String dbAddressLine2;
    @XmlElement(name = "DBCity", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String dbCity;
    @XmlElement(name = "DBStateProvince")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String dbStateProvince;
    @XmlElement(name = "DBCountryCode", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String dbCountryCode;
    @XmlElement(name = "DBPostalCode", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String dbPostalCode;
    @XmlElement(name = "DBCountry")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String dbCountry;

    /**
     * Gets the value of the localeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocaleCode() {
        return localeCode;
    }

    /**
     * Sets the value of the localeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocaleCode(String value) {
        this.localeCode = value;
    }

    /**
     * Gets the value of the primaryAddressIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link TrueFalseType }
     *     
     */
    public TrueFalseType getPrimaryAddressIndicator() {
        return primaryAddressIndicator;
    }

    /**
     * Sets the value of the primaryAddressIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrueFalseType }
     *     
     */
    public void setPrimaryAddressIndicator(TrueFalseType value) {
        this.primaryAddressIndicator = value;
    }

    /**
     * Gets the value of the dbAddressLine property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDBAddressLine() {
        return dbAddressLine;
    }

    /**
     * Sets the value of the dbAddressLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDBAddressLine(String value) {
        this.dbAddressLine = value;
    }

    /**
     * Gets the value of the dbAddressLine2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDBAddressLine2() {
        return dbAddressLine2;
    }

    /**
     * Sets the value of the dbAddressLine2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDBAddressLine2(String value) {
        this.dbAddressLine2 = value;
    }

    /**
     * Gets the value of the dbCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDBCity() {
        return dbCity;
    }

    /**
     * Sets the value of the dbCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDBCity(String value) {
        this.dbCity = value;
    }

    /**
     * Gets the value of the dbStateProvince property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDBStateProvince() {
        return dbStateProvince;
    }

    /**
     * Sets the value of the dbStateProvince property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDBStateProvince(String value) {
        this.dbStateProvince = value;
    }

    /**
     * Gets the value of the dbCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDBCountryCode() {
        return dbCountryCode;
    }

    /**
     * Sets the value of the dbCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDBCountryCode(String value) {
        this.dbCountryCode = value;
    }

    /**
     * Gets the value of the dbPostalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDBPostalCode() {
        return dbPostalCode;
    }

    /**
     * Sets the value of the dbPostalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDBPostalCode(String value) {
        this.dbPostalCode = value;
    }

    /**
     * Gets the value of the dbCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDBCountry() {
        return dbCountry;
    }

    /**
     * Sets the value of the dbCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDBCountry(String value) {
        this.dbCountry = value;
    }

}
