//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.11.20 at 09:18:30 AM CET 
//


package cz.direct.finance.connector.deloitte.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TransmissionHeader_1000Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransmissionHeader_1000Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="TransRecordHeader" type="{}TransRecordHeaderType"/>
 *         &lt;element name="ProcessingStartDate" type="{}MasterCardDateType" minOccurs="0"/>
 *         &lt;element name="ProcessingStartTime" type="{}MasterCardTimeType" minOccurs="0"/>
 *         &lt;element name="ProcessingEndDate" type="{}MasterCardDateType" minOccurs="0"/>
 *         &lt;element name="ProcessingEndTime" type="{}MasterCardTimeType" minOccurs="0"/>
 *         &lt;element name="FileReferenceNum" type="{}Char24Type"/>
 *         &lt;element name="CDFVersionNum" type="{}CDFVersionType"/>
 *         &lt;element name="RunModeIndicator" type="{}RunModeType"/>
 *         &lt;element name="ProcessorNum" type="{}Id11Type"/>
 *         &lt;element name="ProcessorName" type="{}Char35Type" minOccurs="0"/>
 *         &lt;element name="MasterCardFileSequenceNum" type="{}FileSequenceNumberType" minOccurs="0"/>
 *         &lt;element name="MasterCardResetNum" type="{}Numeric6Type" minOccurs="0"/>
 *         &lt;element name="InboundSourceFormat" type="{}Char5Type" minOccurs="0"/>
 *         &lt;element name="ValidationFootPrint" type="{}Char200Type" minOccurs="0"/>
 *         &lt;element name="SchemaVersionNum" type="{}SchemaVersionNumType"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransmissionHeader_1000Type", propOrder = {

})
public class TransmissionHeader1000Type {

    @XmlElement(name = "TransRecordHeader", required = true)
    protected TransRecordHeaderType transRecordHeader;
    @XmlElement(name = "ProcessingStartDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar processingStartDate;
    @XmlElement(name = "ProcessingStartTime")
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar processingStartTime;
    @XmlElement(name = "ProcessingEndDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar processingEndDate;
    @XmlElement(name = "ProcessingEndTime")
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar processingEndTime;
    @XmlElement(name = "FileReferenceNum", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String fileReferenceNum;
    @XmlElement(name = "CDFVersionNum", required = true, defaultValue = "3.00")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String cdfVersionNum;
    @XmlElement(name = "RunModeIndicator", required = true, defaultValue = "P")
    @XmlSchemaType(name = "token")
    protected RunModeType runModeIndicator;
    @XmlElement(name = "ProcessorNum", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String processorNum;
    @XmlElement(name = "ProcessorName")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String processorName;
    @XmlElement(name = "MasterCardFileSequenceNum")
    @XmlSchemaType(name = "unsignedInt")
    protected Long masterCardFileSequenceNum;
    @XmlElement(name = "MasterCardResetNum")
    @XmlSchemaType(name = "unsignedInt")
    protected Long masterCardResetNum;
    @XmlElement(name = "InboundSourceFormat")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String inboundSourceFormat;
    @XmlElement(name = "ValidationFootPrint")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String validationFootPrint;
    @XmlElement(name = "SchemaVersionNum", required = true)
    protected String schemaVersionNum;

    /**
     * Gets the value of the transRecordHeader property.
     * 
     * @return
     *     possible object is
     *     {@link TransRecordHeaderType }
     *     
     */
    public TransRecordHeaderType getTransRecordHeader() {
        return transRecordHeader;
    }

    /**
     * Sets the value of the transRecordHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransRecordHeaderType }
     *     
     */
    public void setTransRecordHeader(TransRecordHeaderType value) {
        this.transRecordHeader = value;
    }

    /**
     * Gets the value of the processingStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getProcessingStartDate() {
        return processingStartDate;
    }

    /**
     * Sets the value of the processingStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setProcessingStartDate(XMLGregorianCalendar value) {
        this.processingStartDate = value;
    }

    /**
     * Gets the value of the processingStartTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getProcessingStartTime() {
        return processingStartTime;
    }

    /**
     * Sets the value of the processingStartTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setProcessingStartTime(XMLGregorianCalendar value) {
        this.processingStartTime = value;
    }

    /**
     * Gets the value of the processingEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getProcessingEndDate() {
        return processingEndDate;
    }

    /**
     * Sets the value of the processingEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setProcessingEndDate(XMLGregorianCalendar value) {
        this.processingEndDate = value;
    }

    /**
     * Gets the value of the processingEndTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getProcessingEndTime() {
        return processingEndTime;
    }

    /**
     * Sets the value of the processingEndTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setProcessingEndTime(XMLGregorianCalendar value) {
        this.processingEndTime = value;
    }

    /**
     * Gets the value of the fileReferenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileReferenceNum() {
        return fileReferenceNum;
    }

    /**
     * Sets the value of the fileReferenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileReferenceNum(String value) {
        this.fileReferenceNum = value;
    }

    /**
     * Gets the value of the cdfVersionNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCDFVersionNum() {
        return cdfVersionNum;
    }

    /**
     * Sets the value of the cdfVersionNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCDFVersionNum(String value) {
        this.cdfVersionNum = value;
    }

    /**
     * Gets the value of the runModeIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link RunModeType }
     *     
     */
    public RunModeType getRunModeIndicator() {
        return runModeIndicator;
    }

    /**
     * Sets the value of the runModeIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link RunModeType }
     *     
     */
    public void setRunModeIndicator(RunModeType value) {
        this.runModeIndicator = value;
    }

    /**
     * Gets the value of the processorNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessorNum() {
        return processorNum;
    }

    /**
     * Sets the value of the processorNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessorNum(String value) {
        this.processorNum = value;
    }

    /**
     * Gets the value of the processorName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessorName() {
        return processorName;
    }

    /**
     * Sets the value of the processorName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessorName(String value) {
        this.processorName = value;
    }

    /**
     * Gets the value of the masterCardFileSequenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMasterCardFileSequenceNum() {
        return masterCardFileSequenceNum;
    }

    /**
     * Sets the value of the masterCardFileSequenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMasterCardFileSequenceNum(Long value) {
        this.masterCardFileSequenceNum = value;
    }

    /**
     * Gets the value of the masterCardResetNum property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMasterCardResetNum() {
        return masterCardResetNum;
    }

    /**
     * Sets the value of the masterCardResetNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMasterCardResetNum(Long value) {
        this.masterCardResetNum = value;
    }

    /**
     * Gets the value of the inboundSourceFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInboundSourceFormat() {
        return inboundSourceFormat;
    }

    /**
     * Sets the value of the inboundSourceFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInboundSourceFormat(String value) {
        this.inboundSourceFormat = value;
    }

    /**
     * Gets the value of the validationFootPrint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidationFootPrint() {
        return validationFootPrint;
    }

    /**
     * Sets the value of the validationFootPrint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidationFootPrint(String value) {
        this.validationFootPrint = value;
    }

    /**
     * Gets the value of the schemaVersionNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchemaVersionNum() {
        return schemaVersionNum;
    }

    /**
     * Sets the value of the schemaVersionNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchemaVersionNum(String value) {
        this.schemaVersionNum = value;
    }

}
