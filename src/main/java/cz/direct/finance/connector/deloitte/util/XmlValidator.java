package cz.direct.finance.connector.deloitte.util;

import cz.direct.finance.connector.deloitte.config.ConfigurationProperties;
import org.springframework.beans.factory.annotation.Autowired;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;



public class XmlValidator {

    public static void validateAgainstXSD(File xmlFile, String schemaPath) {
        try {

//            ClassLoader classLoader = ClassLoader.getSystemClassLoader();
//            InputStream xsd = classLoader.getResourceAsStream("schema.xsd");

            InputStream xsd = new FileInputStream(schemaPath);

            InputStream xml = new FileInputStream(xmlFile);

            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new StreamSource(xsd));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(xml));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
