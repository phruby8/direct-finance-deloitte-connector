package cz.direct.finance.connector.deloitte.service;

/**
 * is thrown when transaction should not be included in the file but want to continue processing for the next transaction
 */
public class ControlledSkipException extends RuntimeException {

    public ControlledSkipException(String message) {
        super(message);
    }
}
