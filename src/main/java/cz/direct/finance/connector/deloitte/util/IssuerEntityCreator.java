package cz.direct.finance.connector.deloitte.util;

import cz.direct.finance.connector.deloitte.model.CardResponse;
import cz.direct.finance.connector.deloitte.model.DataToParse;
import cz.direct.finance.connector.deloitte.model.User;
import cz.direct.finance.connector.deloitte.xsd.AccountEntityType;
import cz.direct.finance.connector.deloitte.xsd.IssuerEntityType;
import cz.direct.finance.connector.deloitte.model.Transaction;
import cz.direct.finance.connector.deloitte.xsd.CorporateEntityType;


public class IssuerEntityCreator {

    public static IssuerEntityType create(Transaction transaction, DataToParse data, String companyUid) {

        IssuerEntityType issuerEntityType = new IssuerEntityType();
        issuerEntityType.setICANumber("18171");
        issuerEntityType.setIssuerNumber("18171");

        CorporateEntityType corporateEntityType = new CorporateEntityType();
        corporateEntityType.setCorporationNumber(companyUid);

        for (int i = 0; i < transaction.getUserIdList().length; i++) {
            User user = data.getUsers().get(transaction.getUserIdList()[i]);
            CardResponse card =  data.getCards().get(transaction.getCardId());
            AccountEntityType accountEntityType = AccountEntityCreator.create(transaction, user, card);
            corporateEntityType.getAccountEntity().add(accountEntityType);
        }

        issuerEntityType.getCorporateEntity().add(corporateEntityType);

        return issuerEntityType;
    }
}
