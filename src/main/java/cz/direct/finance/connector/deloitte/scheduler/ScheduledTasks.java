package cz.direct.finance.connector.deloitte.scheduler;

import cz.direct.finance.connector.deloitte.service.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class ScheduledTasks {

    @Autowired
    private TaskService taskService;

    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    @Scheduled(fixedRate = 1000 * 60 * 60) // runs every hour
    private void createXmlFile() {
        try {
            log.info("Starting process.");
            taskService.process();
            log.info("processed successfully.");
        } catch (Exception e) {
            log.error("error during processing Transaction", e);
        }
    }
}