package cz.direct.finance.connector.deloitte.model.rest;

import cz.direct.finance.connector.deloitte.model.CardResponse;

import java.util.List;

public class GetCardsResponse {

    List<CardResponse> cardList;

    public List<CardResponse> getCardList() {
        return cardList;
    }

    @Override
    public String toString() {
        return "GetCardsResponse{" +
                "cardList=" + cardList +
                '}';
    }
}
