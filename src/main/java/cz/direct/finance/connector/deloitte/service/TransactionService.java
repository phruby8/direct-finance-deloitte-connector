package cz.direct.finance.connector.deloitte.service;

import cz.direct.finance.connector.deloitte.model.rest.GetTransactionResponse;
import cz.direct.finance.connector.deloitte.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class TransactionService {

    @Autowired
    private RestApiService restApiService;

    @Value("${date.from}")
    private String dateFrom;

    @Value("${date.to}")
    private String dateTo;

    private static final Logger log = LoggerFactory.getLogger(TransactionService.class);

    List<Transaction> getTransactionList(int attempt) throws RestClientException {

        ZoneId zoneId = ZoneId.of("Europe/Prague");
        Instant instant = Instant.now();
        ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, zoneId);

        // set default values for case they are not overridden by xml properties
        ZonedDateTime zdtFrom = zdt.toLocalDate().atStartOfDay(zoneId).minusDays(1);
        ZonedDateTime zdtTo = zdt.toLocalDate().atStartOfDay(zoneId).minusDays(0);

        // get date from xml properties if exists
        try {
            if (dateFrom != null && !"".equals(dateFrom)) {
                zdtFrom = LocalDate.parse(dateFrom).atStartOfDay(zoneId);
            }
        } catch (Exception e) {
            log.warn("Exception during parsing date", e);
        }
        try {
            if (dateTo != null && !"".equals(dateTo)) {
                zdtTo = LocalDate.parse(dateTo).atStartOfDay(zoneId);
            }
        } catch (Exception e) {
            log.warn("Exception during parsing date", e);
        }

        List<Transaction> transactions = new ArrayList<>();
        HttpEntity<GetTransactionResponse> response;
        String offset = null;

        do {
            try {
                response = restApiService.getTranstactions(offset, zdtFrom, zdtTo);
                transactions.addAll(Arrays.asList(response.getBody().getTransactionList()));
                offset = response.getBody().getNextOffsetToken();

            } catch (ResourceAccessException e) {
                if (attempt < 3) {
                    log.warn("Connection timeout. Attempt no " + (attempt + 1));
                    return getTransactionList(++attempt);
                } else {
                    log.error("Connection timeout.", e);
                    throw e;
                }
            } catch (RestClientException e) {
                log.error("error during receiving transaction with offset " + offset + ". Message: "
                        + (e instanceof HttpClientErrorException ? ((HttpClientErrorException) e).getResponseBodyAsString() : e.getMessage()), e);
                throw e;
            }
        } while (offset != null);

        return transactions;
    }
}
