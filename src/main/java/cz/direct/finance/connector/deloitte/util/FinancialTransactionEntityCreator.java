package cz.direct.finance.connector.deloitte.util;

import cz.direct.finance.connector.deloitte.model.Transaction;
import cz.direct.finance.connector.deloitte.service.ControlledSkipException;
import cz.direct.finance.connector.deloitte.xsd.CardAcceptor5001Type;
import cz.direct.finance.connector.deloitte.xsd.CurrencyAmountType;
import cz.direct.finance.connector.deloitte.xsd.FinancialTransaction5000Type;
import cz.direct.finance.connector.deloitte.xsd.FinancialTransactionEntityType;
import cz.direct.finance.connector.deloitte.xsd.SignCodeType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;


class FinancialTransactionEntityCreator {

    static FinancialTransactionEntityType create(Transaction transaction) {

        FinancialTransactionEntityType financialTransactionEntityType = new FinancialTransactionEntityType();

        CardAcceptor5001Type cardAcceptor5001Type = new CardAcceptor5001Type();
        cardAcceptor5001Type.setAcquiringICA("xxxxx");
        cardAcceptor5001Type.setCardAcceptorStreetAddress(getLocation(transaction.getMerchantLocation())); // TODO Street Address
        cardAcceptor5001Type.setCardAcceptorCity("xxxxx"); // TODO City
        cardAcceptor5001Type.setCardAcceptorStateProvince("UNK");
        cardAcceptor5001Type.setCardAcceptorLocationPostalCode("xxxxx"); // TODO Zip Code
        cardAcceptor5001Type.setCardAcceptorCountryCode(IsoCountryConverter.getIsoCountryCode(transaction.getMerchantCountry()));
        cardAcceptor5001Type.setCardAcceptorBusinessCode(transaction.getMcc() != null ? transaction.getMcc() : "0000");
        cardAcceptor5001Type.setCardAcceptorName(transaction.getMerchantName()); // merchant name
        financialTransactionEntityType.setCardAcceptor5001(cardAcceptor5001Type);

        cardAcceptor5001Type.setFinancialRecordHeader(HeaderCreator.createFinRecHeader());

        FinancialTransaction5000Type financialTransaction5000Type = new FinancialTransaction5000Type();
        financialTransaction5000Type.setProcessorTransactionId(transaction.getId()); // transaction id
        financialTransaction5000Type.setPostingDate(DateParser.toXMLGregCal(transaction.getSettlementDate() != null ? transaction.getSettlementDate() : transaction.getTransactionDate())); // settlement date
        financialTransaction5000Type.setTransactionDate(DateParser.toXMLGregCal(transaction.getTransactionDate())); // date
        financialTransaction5000Type.setProcessingDate(DateParser.toXMLGregCal(LocalDate.now())); // processingDate
        financialTransaction5000Type.setAcquirerReferenceData("--");
        financialTransaction5000Type.setCardHolderTransactionType(getBusinessCode(transaction.getTransactionType(), transaction.getId()));
        financialTransaction5000Type.setPostedCurrencyConversionDate(JulianConverter.toDate(transaction.getTransactionDate(), transaction.getSettlementDate()));
        financialTransaction5000Type.setPostedConversionRate(new BigDecimal("1.0"));
        financialTransaction5000Type.setDebitOrCreditIndicator(getSign(transaction.getSignedAmount()));
        financialTransaction5000Type.setOriginalCurrencyCode(AmountInOriginalCurrency.getIsoCurrencyCode(transaction.getOriginalCurrency()));
        financialTransaction5000Type.setPostedCurrencyCode("203");

        // original amount
        CurrencyAmountType currencyAmountType = new CurrencyAmountType();
        currencyAmountType.setValue(AmountInOriginalCurrency.adjustValue(transaction.getOriginalCurrency(), transaction.getOriginalAmount()));
        currencyAmountType.setCurrencyCode(AmountInOriginalCurrency.getIsoCurrencyCode(transaction.getOriginalCurrency()));
        currencyAmountType.setCurrencyExponent(AmountInOriginalCurrency.getIsoCurrencyExponent(transaction.getOriginalCurrency()));
        currencyAmountType.setCurrencySign(getSign(transaction.getSignedAmount()));  // debit or credit transaction
        financialTransaction5000Type.setAmountInOriginalCurrency(currencyAmountType);

        // posted ammount currency
        CurrencyAmountType amountInPostedCurrency = new CurrencyAmountType();
        amountInPostedCurrency.setValue(Math.abs(transaction.getSignedAmount().longValue() * 100));
        amountInPostedCurrency.setCurrencyCode("203");
        amountInPostedCurrency.setCurrencyExponent((short) 2);
        amountInPostedCurrency.setCurrencySign(getSign(transaction.getSignedAmount())); // debit or credit transaction
        financialTransaction5000Type.setAmountInPostedCurrency(amountInPostedCurrency);

        financialTransaction5000Type.setFinancialRecordHeader(HeaderCreator.createFinRecHeader());
        financialTransactionEntityType.setProcessorTransactionId(transaction.getId());
        financialTransactionEntityType.setFinancialTransaction5000(financialTransaction5000Type);

        return financialTransactionEntityType;
    }

    private static SignCodeType getSign(Double value) {
        return value > 0 ? SignCodeType.C : SignCodeType.D;
    }

    private static BigDecimal createBigDecimal() {
        BigDecimal bd = new BigDecimal("1.0");
        bd = bd.setScale(2, RoundingMode.HALF_EVEN);
        return bd;
    }

    private static String getLocation(String value) {
        if (value == null) {
            return "xxxxx";
        }
        if (value.length() > 50) {
            return value.substring(0, 50);
        }
        return value;
    }


    /**
     *
     * @param transactionType
     * @param transactionId
     * @return
     */
    private static String getBusinessCode(String transactionType, String transactionId) {
        switch (transactionType) {
            case "merchant":
                return "00";
            case "withdrawal":
                return "01";
            case "other":
                return "50";
            default:
                throw new ControlledSkipException("Transaction type '"  + transactionType + "' not putting to the file for transaction id " + transactionId);
        }

    }
}
