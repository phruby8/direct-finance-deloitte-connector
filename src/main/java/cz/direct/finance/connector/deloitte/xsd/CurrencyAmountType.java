//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.11.20 at 09:18:30 AM CET 
//


package cz.direct.finance.connector.deloitte.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for CurrencyAmountType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CurrencyAmountType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;>CurrencyType">
 *       &lt;attribute name="CurrencyCode" use="required" type="{}CurrencyCodeType" />
 *       &lt;attribute name="CurrencyExponent" use="required" type="{}CurrencyExponentType" />
 *       &lt;attribute name="CurrencySign" use="required" type="{}SignCodeType" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CurrencyAmountType", propOrder = {
    "value"
})
public class CurrencyAmountType {

    @XmlValue
    protected long value;
    @XmlAttribute(name = "CurrencyCode", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String currencyCode;
    @XmlAttribute(name = "CurrencyExponent", required = true)
    protected short currencyExponent;
    @XmlAttribute(name = "CurrencySign", required = true)
    protected SignCodeType currencySign;

    /**
     * Gets the value of the value property.
     * 
     */
    public long getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     */
    public void setValue(long value) {
        this.value = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the currencyExponent property.
     * 
     */
    public short getCurrencyExponent() {
        return currencyExponent;
    }

    /**
     * Sets the value of the currencyExponent property.
     * 
     */
    public void setCurrencyExponent(short value) {
        this.currencyExponent = value;
    }

    /**
     * Gets the value of the currencySign property.
     * 
     * @return
     *     possible object is
     *     {@link SignCodeType }
     *     
     */
    public SignCodeType getCurrencySign() {
        return currencySign;
    }

    /**
     * Sets the value of the currencySign property.
     * 
     * @param value
     *     allowed object is
     *     {@link SignCodeType }
     *     
     */
    public void setCurrencySign(SignCodeType value) {
        this.currencySign = value;
    }

}
