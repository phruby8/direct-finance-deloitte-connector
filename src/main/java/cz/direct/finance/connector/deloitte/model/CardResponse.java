package cz.direct.finance.connector.deloitte.model;

public class CardResponse {

    private String cardId;
    private String cardState ;
    private String cardType;
    private String maskedNumber;
    private String embossName;
    private String alias;
    private String expiration;
    private String availableBalance ;
    private String accountingBalance ;
    private String blockedBalance ;
    private String deliveryEstimate;

    public String getCardId() {
        return cardId;
    }

    public String getCardState() {
        return cardState;
    }

    public String getCardType() {
        return cardType;
    }

    public String getMaskedNumber() {
        return maskedNumber;
    }

    public String getEmbossName() {
        return embossName;
    }

    public String getAlias() {
        return alias;
    }

    public String getExpiration() {
        return expiration;
    }

    public String getAvailableBalance() {
        return availableBalance;
    }

    public String getAccountingBalance() {
        return accountingBalance;
    }

    public String getBlockedBalance() {
        return blockedBalance;
    }

    public String getDeliveryEstimate() {
        return deliveryEstimate;
    }

    @Override
    public String toString() {
        return "CardResponse{" +
                "cardId='" + cardId + '\'' +
                ", cardState='" + cardState + '\'' +
                ", cardType='" + cardType + '\'' +
                ", maskedNumber='" + maskedNumber + '\'' +
                ", embossName='" + embossName + '\'' +
                ", alias='" + alias + '\'' +
                ", expiration='" + expiration + '\'' +
                ", availableBalance='" + availableBalance + '\'' +
                ", accountingBalance='" + accountingBalance + '\'' +
                ", blockedBalance='" + blockedBalance + '\'' +
                ", deliveryEstimate='" + deliveryEstimate + '\'' +
                '}';
    }
}
