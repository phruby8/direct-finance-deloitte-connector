package cz.direct.finance.connector.deloitte.model;

import java.util.List;
import java.util.Map;

public class DataToParse {

    private List<Transaction> transactions;
    private Map<String, User> users;
    Map<String, CardResponse> cards;

    public DataToParse() {
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public Map<String, User> getUsers() {
        return users;
    }

    public void setUsers(Map<String, User> users) {
        this.users = users;
    }

    public Map<String, CardResponse> getCards() {
        return cards;
    }

    public void setCards(Map<String, CardResponse> cards) {
        this.cards = cards;
    }
}
