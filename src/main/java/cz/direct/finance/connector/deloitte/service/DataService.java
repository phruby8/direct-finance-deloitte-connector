package cz.direct.finance.connector.deloitte.service;

import cz.direct.finance.connector.deloitte.model.CardResponse;
import cz.direct.finance.connector.deloitte.model.DataToParse;
import cz.direct.finance.connector.deloitte.model.Transaction;
import cz.direct.finance.connector.deloitte.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class DataService {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private UserService userService;

    @Autowired
    private CardService cardService;

    private static final Logger log = LoggerFactory.getLogger(DataService.class);

    DataToParse prepareDataFromApi(){
        DataToParse data = new DataToParse();

        // get transactions
        List<Transaction> transactions = transactionService.getTransactionList(0);
        if (transactions.size() == 0) {
            log.warn("No transaction found for yesterday!");
        }
        data.setTransactions(transactions);

        // get users
        Map<String, User> users = userService.getUsers(transactions);
        data.setUsers(users);

        Map<String, CardResponse> cards = cardService.getCards(0);
        data.setCards(cards);

        log.info("All data successfully received.");
        return data;
    }
}
