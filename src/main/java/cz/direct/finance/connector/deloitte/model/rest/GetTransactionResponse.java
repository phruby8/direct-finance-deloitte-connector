package cz.direct.finance.connector.deloitte.model.rest;

import cz.direct.finance.connector.deloitte.model.Transaction;

import java.util.Arrays;

public class GetTransactionResponse {

    private boolean complete;
    private String nextOffsetToken;
    private Transaction[] transactionList;

    public boolean isComplete() {
        return complete;
    }

    public String getNextOffsetToken() {
        return nextOffsetToken;
    }

    public Transaction[] getTransactionList() {
        return transactionList;
    }

    @Override
    public String toString() {
        return "GetTransactionResponse{" +
                "complete=" + complete +
                ", nextOffsetToken='" + nextOffsetToken + '\'' +
                ", transactionList=" + Arrays.toString(transactionList) +
                '}';
    }
}

