Generate classes from xsd schema
```bash
xjc -d .\src\main\java -p directfinance.cz.xsd .\src\main\resources\schema.xsd
```